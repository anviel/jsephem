# jsephem

A web-based celestial objects tracking application for altazimutal mounted telescopes and radiotelescopes.

## Getting started
Open the _jsephem_en.html_ file in any web browser (it is known to work with Firefox, Chrome and Edge).
At the top is displayed the current UTC date and time and the local sidereal time for the geographical coordinates.
In the middle the astronomical coordinates of a celestial object are shown, using different coordinate systems: equatorial, galactic and local horizontal (in a clock-type display).
At the bottom of the page, under the **Mode** section, a three option radio button enables to choose among three modes:
- _real time_, the clocks are updated every second with the current UTC date and time. This is the actual tracking mode, the local horizontal coordinates being updated with respect to instantaneous Earth rotation;
- _fixed date and time_, the clocks are frozen with a date and time entered in configuration modes;
- _configuration_, in this mode a new **Configuration** area is displayed below.

In the **Configuration** area, an optional fixed date and time can be defined, the geographical coordinates of the observing site can be either entered or selected from a list of pre-defined sites, and the coordinates of a celestial object can be entered.
By clicking either the _Equatorial coordinates_ or the _Galactic coordinates_ button, the desired coordinate system can be used for the celestial object to be tracked. A list of some pre-defined celestial objects is also available.
Once the date and time, geographical coordinates of observing site and astronomical coordinates of a celestial object are available, the configuration mode is left by clicking one of the two options _real time_ or _fixed date and time_.

A french version of the application is available in _jsephem_fr.html_.

Notice that the application can be used completly off-line, simply copy the files in a directory and open the html file with a web browser.
Only the geolocation feature requires to be online.