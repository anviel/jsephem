//-----------------------------------------------------------------------------
// File      : seven_segments.js
// Purpose   : seven segments display widget
// Author    : A. Viel, Club d'Astronomie Jupiter du Roannais
// Created on: 2020-MAR-19
//-----------------------------------------------------------------------------

// Define the visible segments for the ten digits
var DIGITS = new Array(
      parseInt("00111111", 2), // 0
      parseInt("00000011", 2), // 1
      parseInt("01110110", 2), // 2
      parseInt("01100111", 2), // 3
      parseInt("01001011", 2), // 4
      parseInt("01101101", 2), // 5
      parseInt("01111101", 2), // 6
      parseInt("00000111", 2), // 7
      parseInt("01111111", 2), // 8
      parseInt("01101111", 2), // 9
      parseInt("01000000", 2) // minus sign
);
// Define the end points of the 7 segments
var SEGMENTS = new Array(
      [ 1, 1, 1, 2 ],
      [ 1, 0, 1, 1 ],
      [ 0, 0, 1, 0 ],
      [ 0, 0, 0, 1 ],
      [ 0, 1, 0, 2 ],
      [ 0, 2, 1, 2 ],
      [ 0, 1, 1, 1 ]
);

// Constructor of the SevenSegment display object
//
// Arguments:
// ----------
// canvas: a canvas object
// n     : the number of numbers to display, 1, 2 or 3
// height: the height (in pixel) of one digit
// sign  : optional boolean. If true, a negative sign
//         may be displayed in front of the first digit
// threedigit: optional boolean. If true, the first number has 3 digits
function SevenSegment(canvas, n, height, sign=false, threedigit=false)
{
   // Check number
   if (n < 1 || n > 3)
   {
      alert("SevenSegment: number of numbers must be 1, 2 or 3!");
      return;
   }
   this.sign = sign;
   this.threedigit = threedigit;
   // Initialize the numbers
   this.numbers = new Array();
   for(var i = 0; i < n; i++)
   {
      this.numbers[i] = 0
   }
   // Sizes
   this.height = height;
   this.w = 5;        // segment width
   this.space1 = 0.7; // relative space between two digits
   this.space2 = 0.9; // relative space between two numbers
   // Colors
   this.fg = "#000000";  // foreground color
   this.bg = "#ffffff";  // background color
   // Canvas
   this.context = canvas.getContext("2d");
   this.can_width = n*height+(n-1)*(this.space1+this.space2)*height/2;
   if (sign)
   {
      this.can_width += this.space1*height;
   }
   if (threedigit)
   {
      this.can_width += this.space1*height;
   }
   this.can_height = height+20;
   canvas.setAttribute("width", this.can_width);
   canvas.setAttribute("height", this.can_height);
}
SevenSegment.prototype.set_segment_width = function(w)
{
   this.w = w;
}
SevenSegment.prototype.set_color = function(fg, bg)
{
   this.fg = fg;
   this.bg = bg;
}
SevenSegment.prototype.clear_one_digit = function(x, y)
{
   // Clear
   this.context.fillStyle = this.bg;
   this.context.fillRect(x-this.w, y-this.w,
                         this.height/2+2*this.w, this.height+2*this.w);
}
SevenSegment.prototype.draw_one_digit = function(x, y, d)
{
   // Check digit
   if (d < 0 || d > 10)
   {
      alert("draw_one_digit: d argument (="+d+") is not within 0..9!");
      return;
   }
   // Clear
   this.context.fillStyle = this.bg;
   this.context.fillRect(x-this.w, y-this.w,
                         this.height/2+2*this.w, this.height+2*this.w);
   // Draw the segments
   for(var i = 0; i < 7; i++)
   {
      if ((DIGITS[d] & (1 << i)) > 0)
      {
         this.context.fillStyle = this.fg;
         this.context.beginPath();
         if (SEGMENTS[i][0] == SEGMENTS[i][2])
         {  /* vertical segment, hexagon */
            this.context.moveTo(x + this.height/2*SEGMENTS[i][0],
                                y + this.w + this.height/2*SEGMENTS[i][1]);
            this.context.lineTo(x - this.w + this.height/2*SEGMENTS[i][0],
                                y + 2*this.w + this.height/2*SEGMENTS[i][1]);
            this.context.lineTo(x - this.w + this.height/2*SEGMENTS[i][2],
                                y - 2*this.w + this.height/2*SEGMENTS[i][3]);
            this.context.lineTo(x + this.height/2*SEGMENTS[i][2],
                                y - this.w + this.height/2*SEGMENTS[i][3]);
            this.context.lineTo(x + this.w + this.height/2*SEGMENTS[i][2],
                                y - 2*this.w + this.height/2*SEGMENTS[i][3]);
            this.context.lineTo(x + this.w + this.height/2*SEGMENTS[i][0],
                                y + 2*this.w + this.height/2*SEGMENTS[i][1]);
         }
         else if (SEGMENTS[i][1] == SEGMENTS[i][3])
         {  /* horizontal segment, hexagon */
            this.context.moveTo(x + this.w + this.height/2*SEGMENTS[i][0],
                                y + this.height/2*SEGMENTS[i][1]);
            this.context.lineTo(x + 2*this.w + this.height/2*SEGMENTS[i][0],
                                y + this.w + this.height/2*SEGMENTS[i][1]);
            this.context.lineTo(x - 2*this.w + this.height/2*SEGMENTS[i][2],
                                y + this.w + this.height/2*SEGMENTS[i][3]);
            this.context.lineTo(x - this.w + this.height/2*SEGMENTS[i][2],
                                y + this.height/2*SEGMENTS[i][3]);
            this.context.lineTo(x - 2*this.w + this.height/2*SEGMENTS[i][2],
                                y - this.w + this.height/2*SEGMENTS[i][3]);
            this.context.lineTo(x + 2*this.w + this.height/2*SEGMENTS[i][0],
                                y - this.w + this.height/2*SEGMENTS[i][1]);
         }
         this.context.closePath();
         this.context.fill();
      }
   }
}
// Update the displayed numbers and draw
// Arguments:
// ----------
// numbers: an Array of numbers to be updated
// force  : a bool value. If true, all digits are painted.
//          If false, only the updated digits.
SevenSegment.prototype.update_and_draw = function(numbers, force)
{
   // Repaint the background
   if (force)
   {
      this.context.fillStyle = this.bg;
      this.context.fillRect(0, 0, this.can_width, this.can_height);
   }
   var x = 10;  // a small offset to avoid painting on the edge
   if (this.sign)
   {
      if ((numbers[0]*this.numbers[0] <= 0) || force)
      {
         this.clear_one_digit(x, 10);
         if (numbers[0] < 0)
         {
            this.draw_one_digit(x, 10, 10);  // 10 is for minus sign
         }
      }
      x += Math.floor(this.space1*this.height);
   }
   for(var i = 0; i < this.numbers.length; i++)
   {
      var v;
      // Most significant digit
      if (i == 0 && this.threedigit)
      {
         v = Math.floor(Math.abs(numbers[i]) / 100);
         if ((Math.floor(Math.abs(this.numbers[i])/100) != v) || force)
         {
            this.draw_one_digit(x, 10, v);
         }
         x += Math.floor(this.space1*this.height);
         v = Math.floor((Math.abs(numbers[i])-v*100) / 10);
      }
      else
      {
         v = Math.floor(Math.abs(numbers[i]) / 10);
      }
      if ((Math.floor(Math.abs(this.numbers[i])/10) != v) || force)
      {
         this.draw_one_digit(x, 10, v);
      }
      x += Math.floor(this.space1*this.height);

      // Least significant digit
      v = Math.abs(numbers[i]) % 10;
      if ((Math.abs(this.numbers[i]) % 10 != v) || force)
      {
         this.draw_one_digit(x, 10, v);
      }
      x += Math.floor(this.space2*this.height);

      // Update value
      this.numbers[i] = numbers[i];
   }
}
SevenSegment.prototype.draw = function(numbers)
{
   return this.update_and_draw(numbers, true)
}
SevenSegment.prototype.update = function(numbers)
{
   return this.update_and_draw(numbers, false)
}

