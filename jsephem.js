// -----------------------------------------------------------------------------
// File       : jspephem.js
// Purpose    : tracking of celestial object, displaying the local sidereal time
//              and equatorial and local horizontal coordinates of object
// Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
// Created on : 2021-MAR-20
// Modified on: 2021-APR-06
// -----------------------------------------------------------------------------


// A small GeolocationHelper object.
// Constructor takes as argument the input widgets for
// geographical coordinates
function GeolocationHelper(lat_deg, lat_min, lat_sec, lon_deg, lon_min, lon_sec, site_select)
{
   this.lat_deg = lat_deg;
   this.lat_min = lat_min;
   this.lat_sec = lat_sec;
   this.lon_deg = lon_deg;
   this.lon_min = lon_min;
   this.lon_sec = lon_sec;
   this.site_select = site_select;
}
GeolocationHelper.prototype.success = function (position)
{
   //alert("Geolocation succeded!");
   // update the latitude input widgets
   var lat_dms = deg2dms(position.coords.latitude);
   this.lat_deg.value = lat_dms[0].toString();
   this.lat_min.value = lat_dms[1].toString();
   this.lat_sec.value = lat_dms[2].toString();
   // update the longitude input widgets
   var lon_dms = deg2dms(position.coords.longitude);
   this.lon_deg.value = lon_dms[0].toString();
   this.lon_min.value = lon_dms[1].toString();
   this.lon_sec.value = lon_dms[2].toString();
   // reset the site selection list
   this.site_select.selectedIndex = 0;
}
GeolocationHelper.prototype.failure = function (error)
{
   if (error.code != 1)
   {
      alert("Geolocation error " + error.code
            + "\n" +  error.message);
   }
}

// Constants for mode
const REALTIME_MODE = 0
const FIXED_MODE = 1
const CONFIG_MODE = 2

// The main object, featuring a UTC and sidereal clock.
// This constructor receices as argument all the widgets
// of the HTML page it needs to work with.
function Clock(
      display_date, utc_canvas,
      display_lat, display_lon, lst_canvas,
      display_object_name,
      display_ra, display_dec, display_gal_lat, display_gal_lon,
      az_canvas, elev_canvas,
      realtime_radiobutton, fixed_radiobutton, config_radiobutton,
      config_form,
      date_input, time_input,
      lat_deg, lat_min, lat_sec,
      lon_deg, lon_min, lon_sec,
      site_select,
      geoloc_button,
      ra_h, ra_min, ra_sec,
      dec_deg, dec_min, dec_sec,
      celestial_object_select,
      equatorial_choice, galactic_choice,
      gal_lat_deg, gal_lat_min, gal_lat_sec,
      gal_lon_deg, gal_lon_min, gal_lon_sec
      )
{
   // create two display objects
   this.display_date = display_date;
   this.display1 = new SevenSegment(utc_canvas, 3, 100);
   this.display1.set_segment_width(4);
   this.display1.set_color("#75cea3", "#3a277c");
   this.display1.draw(new Array(88, 88, 88));
   this.display_lat = display_lat;
   this.display_lon = display_lon;
   this.display2 = new SevenSegment(lst_canvas, 3, 100);
   this.display2.set_segment_width(4);
   this.display2.set_color("#75cea3", "#3a277c");
   this.display2.draw(new Array(88, 88, 88));
   // create two other display objects for local coordinates
   this.display_object_name = display_object_name;
   this.display_ra = display_ra;
   this.display_dec = display_dec;
   this.display_gal_lat = display_gal_lat;
   this.display_gal_lon = display_gal_lon;
   this.display3 = new SevenSegment(az_canvas, 3, 100, false, true);
   this.display3.set_segment_width(4);
   this.display3.set_color("#75cea3", "#3a277c");
   this.display3.draw(new Array(0, 0, 0));
   this.display4 = new SevenSegment(elev_canvas, 3, 100, true, false);
   this.display4.set_segment_width(4);
   this.display4.set_color("#75cea3", "#3a277c");
   this.display4.draw(new Array(0, 0, 0));
   // refresh every second
   this.clock = window.setInterval(this.update.bind(this), 1000);
   // set callback for the three radio buttons
   this.mode = REALTIME_MODE;
   realtime_radiobutton.onclick = this.realtime_rb_cb.bind(this);
   fixed_radiobutton.onclick = this.fixed_rb_cb.bind(this);
   config_radiobutton.onclick = this.config_rb_cb.bind(this);
   this.config_form = config_form;
   // fixed date & time
   this.fixed_datetime = new Date();
   this.date_input = date_input;
   this.time_input = time_input;
   // Geo. coords
   this.lat_deg = lat_deg;
   this.lat_min = lat_min;
   this.lat_sec = lat_sec;
   this.lon_deg = lon_deg;
   this.lon_min = lon_min;
   this.lon_sec = lon_sec;
   // Site selection
   this.site_select = site_select;
   populate_site_select(this.site_select);
   this.site_select.onchange = this.site_change_cb.bind(this);
   // geolocation
   this.geoloc_button = geoloc_button;
   if (navigator.geolocation)
   {
      this.geoloc_button.onclick = this.geoloc_cb.bind(this);
   }
   else
   {  // not available
      this.geoloc_button.disabled = true;
   }
   // Equato. coords
   this.ra_h = ra_h;
   this.ra_min = ra_min;
   this.ra_sec = ra_sec;
   this.dec_deg = dec_deg;
   this.dec_min = dec_min;
   this.dec_sec = dec_sec;
   // Celestial object selection
   this.celestial_object_select = celestial_object_select;
   populate_object_select(this.celestial_object_select);
   //this.celestial_object_select.onchange = this.celestial_object_change_cb.bind(this);
   // Choosing the type of input coordinates
   this.equatorial_choice = equatorial_choice;
   this.galactic_choice = galactic_choice;
   this.gal_lat_deg = gal_lat_deg;
   this.gal_lat_min = gal_lat_min;
   this.gal_lat_sec = gal_lat_sec;
   this.gal_lon_deg = gal_lon_deg;
   this.gal_lon_min = gal_lon_min;
   this.gal_lon_sec = gal_lon_sec;
}
Clock.prototype.set_clock = function(d)
{
   // set date
   this.display_date.innerHTML =
      d.getUTCFullYear().toString()
      +" / "+twodig(d.getUTCMonth()+1)
      +" / "+twodig(d.getUTCDate());
   // set UTC time
   this.display1.update(
      new Array(d.getUTCHours(), d.getUTCMinutes(), d.getUTCSeconds()));
   // set location geographical coordinates
   this.display_lat.innerHTML =
      sexa2string(this.lat_deg.value, this.lat_min.value, this.lat_sec.value)
   this.display_lon.innerHTML =
      sexa2string(this.lon_deg.value, this.lon_min.value, this.lon_sec.value);
   // set local sidereal time
   this.display2.update(hr2hms(lst(d, parseInt(this.lon_deg.value),
                                      parseInt(this.lon_min.value),
                                      parseInt(this.lon_sec.value))));
   var ra_h, ra_min, ra_sec,
       dec_deg, dec_min, dec_sec,
       gal_lat_deg, gal_lat_min, gal_lat_sec,
       gal_lon_deg, gal_lon_min, gal_lon_sec,
       obj_selected;
   obj_selected = this.celestial_object_select.selectedIndex;
   if (obj_selected == 0)
   {
      // custom selection: get the coordinates from the form fields
      if (this.equatorial_choice.checked)
      {
         // get equatorial coordinates from input fields
         ra_h    = this.ra_h.value;
         ra_min  = this.ra_min.value;
         ra_sec  = this.ra_sec.value;
         dec_deg = this.dec_deg.value;
         dec_min = this.dec_min.value;
         dec_sec = this.dec_sec.value;
         // and convert to galactic coordinates
         var gal_coords = equatorial2galactic(
               ra_h, ra_min, ra_sec,
               dec_deg, dec_min, dec_sec);
         [gal_lat_deg, gal_lat_min, gal_lat_sec] = deg2dms(gal_coords[0]);
         [gal_lon_deg, gal_lon_min, gal_lon_sec] = deg2dms(gal_coords[1]);
      }
      else if (this.galactic_choice.checked)
      {
         // get galactic coordinates
         gal_lat_deg = this.gal_lat_deg.value;
         gal_lat_min = this.gal_lat_min.value;
         gal_lat_sec = this.gal_lat_sec.value;
         gal_lon_deg = this.gal_lon_deg.value;
         gal_lon_min = this.gal_lon_min.value;
         gal_lon_sec = this.gal_lon_sec.value;
         // and convert to equatorial coordinates
         var equ_coords = galactic2equatorial(
               gal_lat_deg, gal_lat_min, gal_lat_sec,
               gal_lon_deg, gal_lon_min, gal_lon_sec
            );
         [ra_h, ra_min, ra_sec] = hr2hms(equ_coords[0]);
         [dec_deg, dec_min, dec_sec] = deg2dms(equ_coords[1]);
      }
   }
   else
   {
      // get equatorial coordinates at date/time d
      var equ_coords = CELESTIAL_OBJECTS[obj_selected].position(d);
      [ra_h, ra_min, ra_sec] = hr2hms(equ_coords[0]);
      [dec_deg, dec_min, dec_sec] = deg2dms(equ_coords[1]);
      // and convert to galactic coordinates
      var gal_coords = equatorial2galactic(
            ra_h, ra_min, ra_sec,
            dec_deg, dec_min, dec_sec);
      [gal_lat_deg, gal_lat_min, gal_lat_sec] = deg2dms(gal_coords[0]);
      [gal_lon_deg, gal_lon_min, gal_lon_sec] = deg2dms(gal_coords[1]);
   }
   // display coordinates
   this.display_object_name.innerHTML = CELESTIAL_OBJECTS[obj_selected].name;
   this.display_ra.innerHTML =
      sexa2string(ra_h, ra_min, ra_sec, false);
   this.display_dec.innerHTML =
      sexa2string(dec_deg, dec_min, dec_sec);
   this.display_gal_lat.innerHTML =
      sexa2string(gal_lat_deg, gal_lat_min, gal_lat_sec);
   this.display_gal_lon.innerHTML =
      sexa2string(gal_lon_deg, gal_lon_min, gal_lon_sec);
   // get and display local coordinates
   var loc = equatorial2local(
         ra_h, ra_min, ra_sec,
         dec_deg, dec_min, dec_sec,
         d,
         parseInt(this.lat_deg.value),
         parseInt(this.lat_min.value),
         parseInt(this.lat_sec.value),
         parseInt(this.lon_deg.value),
         parseInt(this.lon_min.value),
         parseInt(this.lon_sec.value));
   this.display3.update(deg2dms(loc[0]));  // azimuth
   this.display4.update(deg2dms(loc[1]));  // elevation
}
Clock.prototype.update = function()
{
   // incremental update from current date/time
   var d = new Date();
   this.set_clock(d);
}
Clock.prototype.realtime_rb_cb = function ()
{
   // switch to realtime mode, re-starting the clock
   this.mode = REALTIME_MODE;
   this.clock = window.setInterval(this.update.bind(this), 1000);
   this.config_form.hidden = true;
   window.scroll(0, 0);
}
Clock.prototype.fixed_rb_cb = function ()
{
   // switch to fixed date/time mode, stopping the clock
   this.mode = FIXED_MODE;
   window.clearInterval(this.clock);
   this.config_form.hidden = true;
   // get the fixed UTC date/time and apply it
   this.fixed_datetime = new Date();
   this.fixed_datetime.setUTCFullYear(this.date_input.value.substring(0, 4));
   this.fixed_datetime.setUTCMonth(this.date_input.value.substring(5, 7)-1);
   this.fixed_datetime.setUTCDate(this.date_input.value.substring(8, 10));
   this.fixed_datetime.setUTCHours(this.time_input.value.substring(0, 2));
   this.fixed_datetime.setUTCMinutes(this.time_input.value.substring(3, 5));
   this.fixed_datetime.setUTCSeconds(0);
   this.set_clock(this.fixed_datetime);
   window.scroll(0, 0);
}
Clock.prototype.config_rb_cb = function ()
{
   // switch to configuration mode, stopping the clock
   this.mode = CONFIG_MODE;
   window.clearInterval(this.clock);
   // open the configuration panel
   this.config_form.hidden = false;
   window.scroll(0, 1000);
}
Clock.prototype.site_change_cb = function ()
{
   var i = this.site_select[this.site_select.selectedIndex].value;
   this.lat_deg.value = SITES[i].lat_deg.toString();
   this.lat_min.value = SITES[i].lat_min.toString();
   this.lat_sec.value = SITES[i].lat_sec.toString();
   this.lon_deg.value = SITES[i].lon_deg.toString();
   this.lon_min.value = SITES[i].lon_min.toString();
   this.lon_sec.value = SITES[i].lon_sec.toString();
}
Clock.prototype.geoloc_cb = function ()
{
   var geoloc = new GeolocationHelper(
                 this.lat_deg, this.lat_min, this.lat_sec,
                 this.lon_deg, this.lon_min, this.lon_sec,
                 this.site_select);
   navigator.geolocation.getCurrentPosition(
         geoloc.success.bind(geoloc),
         geoloc.failure.bind(geoloc));
}
