// -----------------------------------------------------------------------------
// File       : computation.js
// Purpose    : low precision astronomical computations
// Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
// Created on : 2021-APR-06
// Modified on: 2021-APR-06
// Reference  : Peter Duffett-Smith, Practical Astronomy with your Calculator,
//              3rd ed., Cambridge University Press, 1995.
// -----------------------------------------------------------------------------

// -------------------------------------
//
// Mathematical utility functions
//
// -------------------------------------

// degree to radian
function deg2rad(x)
{
   return x * Math.PI/180;
}

// radian to degree
function rad2deg(x)
{
   return x * 180 / Math.PI;
}

function mod360(x)
{
   return x - 360*Math.floor(x/360);
}

// Evaluate a polynomial:
// p[0] + p[1]*x + ... + p[p.length-1].x**(p.length-1)
function polynomial(p, x)
{
   var xa = 1;
   var r = 0;
   for(var i = 0; i < p.length; i++)
   {
      r += p[i] * xa;
      xa *= x;
   }
   return r;
}

// Write a number with at least two digits,
// putting a zero in front of single digit number
function twodig(x)
{
   if (Math.abs(x) < 10)
   {
      var s = "0" + Math.abs(x).toString();
      return x < 0 ? "-"+s : s;
   }
   else
   {
      return x.toString();
   }
}

// Convert a number from sexagesimal to string
function sexa2string(x1, x2, x3, degr=true)
{
   if (degr)
   {  // degree, arcminute, arcsecond
      return twodig(x1)+"� "+twodig(x2)+"' "+twodig(x3)+"''";
   }
   else
   {  // hour, minute, second
      return twodig(x1)+"h "+twodig(x2)+"' "+twodig(x3)+"''";
   }
}

// Product of a 3x3-matrix (defined by its columns) by a 3-elements vector
function column_matrix_dot_vector(a1, a2, a3, x)
{
   return new Array(
         a1[0]*x[0] + a2[0]*x[1] + a3[0]*x[2],
         a1[1]*x[0] + a2[1]*x[1] + a3[1]*x[2],
         a1[2]*x[0] + a2[2]*x[1] + a3[2]*x[2]
      );
}

// Difference between two 3-elements vectors
function vector_minus_vector(a, b)
{
   return new Array(
         a[0] - b[0],
         a[1] - b[1],
         a[2] - b[2]
      );
}

// Norm of a 3-elements vector
function norm_vect(a)
{
   return Math.sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
}

// -------------------------------------
//
// Time and angle related functions
//
// -------------------------------------

// DELTAT = TDT - UTC
const DELTAT = 71.80  // s

// decimal hour to hour, minute, second
function hr2hms(h)
{
   var hr  = Math.floor(h)
   var min = Math.floor(60*(h - hr));
   var sec = Math.floor(60*(60*(h - hr) - min));
   return new Array(hr, min, sec);
}

// degree, minute, second to decimal degree
function dms2deg(deg, min, sec)
{
   var sign = 1;
   if (deg < 0)
   {
      sign = -1;
   }
   return sign*(Math.abs(deg) + min/60 + sec/3600);
}

// hour, minute, second to decimal hour
function hms2hr(h, m, s)
{
   // same formula as decimal degree to degree, minute, second
   return dms2deg(h, m, s);
}

// decimal degree to degree, minute, second
function deg2dms(x)
{
   var sign = 1;
   if (x < 0)
   {
      sign = -1;
   }
   var v = Math.abs(x);
   var deg = Math.floor(v);
   var min = Math.floor(60*(v - deg));
   var sec = Math.floor(60*(60*(v - deg) - min));
   return new Array(sign*deg, min, sec);
}

// Compute Julian Day Number from calendar date
//
// Arguments:
// ----------
// dt: a Date object
//
// Returns:
// --------
// the Julian Day number
function date2jd(dt)
{
   var day = dt.getUTCDate();
   var month = dt.getUTCMonth()+1;
   var year = dt.getUTCFullYear();
   var fracday = dt.getUTCHours()/24
                 + dt.getUTCMinutes()/1440
                 + dt.getUTCSeconds()/86400;

   var yprime, mprime;
   if (month == 1 || month == 2)
   {
      yprime = year-1;
      mprime = month+12;
   }
   else
   {
      yprime = year;
      mprime = month;
   }

   var A = Math.floor(yprime/100);
   var B = 2 - A + Math.floor(A/4);
   var C = Math.floor(365.25*yprime);
   var D = Math.floor(30.6001 * (mprime+1));

   return B + C + D + day + fracday + 1720994.5;
}

// number of julian centuries since J2000.0 epoch
function jd2jcent(jd)
{
   return (jd - 2451545.0) / 36525.0;
}

// Compute the Greenwich Sidereal Time from calendar date
//
// Arguments:
// ----------
// dt: a Date object
// Returns:
// --------
// the GST, in sidereal hours
function gst(dt)
{
   var day = dt.getUTCDate();
   var month = dt.getUTCMonth();
   var year = dt.getUTCFullYear();
   var ut = dt.getUTCHours()
            + dt.getUTCMinutes()/60
            + dt.getUTCSeconds()/3600;

   // get JD at UTC midnight
   var d0 = new Date();
   d0.setUTCFullYear(year);
   d0.setUTCMonth(month);
   d0.setUTCDate(day);
   d0.setUTCHours(0);
   d0.setUTCMinutes(0);
   d0.setUTCSeconds(0);
   var jd = date2jd(d0);

   // apply earth rotation and reduce to the range 0-24
   var T0 = polynomial(new Array(6.697374558, 2400.051336, 0.000025862),
                       jd2jcent(jd));
   T0 -= 24*Math.floor(T0 / 24);

   // get gst and reduce to the range 0-24
   var gst = 1.002737909*ut + T0;
   gst -= 24*Math.floor(gst / 24);

   return gst;
}

// Compute the local sidereal time for a given longitude
//
// Arguments:
// ----------
// dt     : a Date object
// lon_deg: longitude, part in degrees, >0 for East, <0 for West
// lon_min: longitude, part in arc minute
// lon_sec: longitude, part in arc second
//
// Returns:
// --------
// the LST, in sidereal hours
function lst(dt, lon_deg, lon_min, lon_sec)
{
   var t = gst(dt)
   t += dms2deg(lon_deg, lon_min, lon_sec)/15
   t -= 24*Math.floor(t / 24);
   return t;
}

// -------------------------------------
//
// Coordinate related functions
//
// -------------------------------------

// Correct equatorial coordinates from J2000.0 epoch
// to mean coordinates for epoch at date dt
//
// Arguments:
// ----------
// dt   : a Date object
// ra0  : right ascension, in sideral hour
// dec0 : declination, in decimal degree
//
// Returns:
// --------
// a two element array containing the corrected right ascension
// in sidereal hour and the corrected declination, in decimal degree.
function correct_precession(dt, ra0, dec0)
{
   // get the coordinates, convert in radian
   var alpha = deg2rad(15*ra0);
   var delta = deg2rad(dec0);
   // get Julian date of current date/time
   var jd = date2jd(dt);
   // number of julian centuries since J2000.0 epoch
   var T = jd2jcent(jd);
   // IAU 1976 precesion angles, convert in radian
   var zeta  = deg2rad(polynomial(new Array(0, 2306.2181, 0.30188, 0.017998), T)/3600);
   var z     = deg2rad(polynomial(new Array(0, 2306.2181, 1.09468, 0.018203), T)/3600);
   var theta = deg2rad(polynomial(new Array(0, 2004.3109, 0.42665, 0.041833), T)/3600);
   // pre-compute some trigonometry functions
   var cd0 = Math.cos(delta);
   var sd0 = Math.sin(delta);
   var cth = Math.cos(theta);
   var sth = Math.sin(theta);
   var caz = Math.cos(alpha+zeta);
   var saz = Math.sin(alpha+zeta);
   // compute corrected coordinates
   var ra1 = rad2deg(z+Math.atan2(cd0*saz, cth*cd0*caz-sth*sd0))/15;
   ra1 -= 24*Math.floor(ra1/24);
   var dec1 = rad2deg(Math.asin(sth*cd0*caz+cth*sd0));
   return new Array(ra1, dec1);
}

// Convert equatorial coordinates into local horizontal coordinates
//
// Arguments:
// ----------
// ra_h   : right ascension, hour part
// ra_min : right ascension, minute part
// ra_sec : right ascension, second part
// dec_h  : declination, degree part
// dec_min: declination, minute part
// dec_sec: declination, second part
// dt     : a Date object
// lat_deg: latitude, degree part
// lat_min: latitude, minute part
// lat_sec: latitude, second part
// lon: longitude, degree part
// lon: longitude, minute part
// lon: longitude, second part
//
// Returns:
// ----------
// a two-element array containing the azimuth and elevation,
// expressed in decimal degree
function equatorial2local(
      ra_h, ra_min, ra_sec,
      dec_deg, dec_min, dec_sec,
      dt,
      lat_deg, lat_min, lat_sec,
      lon_deg, lon_min, lon_sec)
{
   // convert to decimal degree
   var dec0 = dms2deg(dec_deg, dec_min, dec_sec);
   var phi  = dms2deg(lat_deg, lat_min, lat_sec);

   // hour angle, in degree
   var ra0  = hms2hr(ra_h, ra_min, ra_sec);
   var H = 15 * (lst(dt, lon_deg, lon_min, lon_sec) - ra0);
   // correct for the difference between TDT and UTC
   //H += 15 * DELTAT/3600 * 2400.051336/36525.0;
   // set into the range 0-360
   H = mod360(H);

   // pre-compute all the trigonometric functions
   var cd   = Math.cos(deg2rad(dec0));
   var sd   = Math.sin(deg2rad(dec0));
   var cphi = Math.cos(deg2rad(phi));
   var sphi = Math.sin(deg2rad(phi));
   var cH   = Math.cos(deg2rad(H));
   var sH   = Math.sin(deg2rad(H));

   // get the two angles, in decimal degrees
   var elev = rad2deg(Math.asin(sd*sphi+cd*cphi*cH));
   var sel  = Math.sin(deg2rad(elev));
   var az   = rad2deg(Math.atan2(-cd*cphi*sH, sd-sphi*sel));
   az = mod360(az);

   return new Array(az, elev);
}

// Return the mean obliquity of the ecliptic.
// Low precision, but good enough for most uses. [Meeus-1998: equation 22.2].
// Accuracy is 1" over 2000 years and 10" over 4000 years.
function obliquity(jd)
{
   const _el0 = new Array(
      dms2deg(23, 26, 21.448),
      dms2deg(0, 0, -46.8150),
      dms2deg(0, 0, -0.00059),
      dms2deg(0, 0, 0.001813));
   var T = jd2jcent(jd);
   return polynomial(_el0, T);
}

// Convert from ecliptic coordinates to equatorial coordinates
//
// Arguments:
// ----------
// jd : the Julian day number, used for computing the obliquity
// lat: the ecliptic latitude, in decimal degree
// lon: the ecliptic longitude, in decimal degree
//
// Returns:
// --------
// A two-element array containing the right ascension in sidereal hour
// and declination angle, expressed in decimal degree
function ecliptic2equatorial(jd, lat, lon)
{
   var obli = obliquity(jd);

   // pre-compute some trigonometric factors
   var cose = Math.cos(deg2rad(obli));
   var sine = Math.sin(deg2rad(obli));
   var coslo = Math.cos(deg2rad(lon));
   var sinlo = Math.sin(deg2rad(lon));
   var cosla = Math.cos(deg2rad(lat));
   var sinla = Math.sin(deg2rad(lat));

   // compute right ascension and declination angles, in decimal degree
   var ra = rad2deg(
      Math.atan2(sinlo * cose - sinla / cosla * sine, coslo))/15;
   ra -= 24*Math.floor(ra/24);
   var dec = rad2deg(Math.asin(sinla * cose + cosla * sine * sinlo));
   return new Array(ra, dec);
}

// Convert equatorial coordinates into galactic coordinates
//
// Arguments:
// ----------
// ra_h   : right ascension, hour part
// ra_min : right ascension, minute part
// ra_sec : right ascension, second part
// dec_h  : declination, degree part
// dec_min: declination, minute part
// dec_sec: declination, second part
//
// Returns:
// ----------
// a two-element array containing the galactic latitude
// and galactic longitude expressed in decimal degree
function equatorial2galactic(
      ra_h, ra_min, ra_sec,
      dec_deg, dec_min, dec_sec)
{
   // convert to decimal degree
   var alpha = 15*hms2hr(ra_h, ra_min, ra_sec);
   var delta = dms2deg(dec_deg, dec_min, dec_sec);

   // north galactic pole
   const alphaNGP = dms2deg(192, 15, 0);
   const deltaNGP = dms2deg(27, 24, 0);

   // longitude of ascending node of galactic plane
   const lonGP = 33;

   // pre-computed trigonometric functions
   var cdNGP = Math.cos(deg2rad(deltaNGP));
   var sdNGP = Math.sin(deg2rad(deltaNGP));
   var calf = Math.cos(deg2rad(alpha-alphaNGP));
   var salf = Math.sin(deg2rad(alpha-alphaNGP));
   var cdel = Math.cos(deg2rad(delta));
   var sdel = Math.sin(deg2rad(delta));

   // galactic latitude
   var gal_lat = rad2deg(Math.asin(cdel*cdNGP*calf + sdel*sdNGP));
   var sgal_lat = Math.sin(deg2rad(gal_lat));

   // galactic longitude
   var gal_lon = lonGP + rad2deg(Math.atan2(
         sdel-sgal_lat*sdNGP, cdel*salf*cdNGP
      ));
   gal_lon = mod360(gal_lon);

   return new Array(gal_lat, gal_lon);
}

// Convert galactic coordinates into equatorial coordinates
//
// Arguments:
// ----------
// gal_lat_deg: the galactic latitude, degree part
// gal_lat_min: the galactic latitude, minute part
// gal_lat_sec: the galactic latitude, second part
// gal_lon_deg: the galactic longitude, degree part
// gal_lon_min: the galactic longitude, minute part
// gal_lon_sec: the galactic longitude, second part
//
// Returns:
// --------
// A two-element array containing the right ascension
// and the declination, expressed in decimal degree
function galactic2equatorial(
      gal_lat_deg, gal_lat_min, gal_lat_sec,
      gal_lon_deg, gal_lon_min, gal_lon_sec)
{
   // convert to decimal degree
   var gal_lat = dms2deg(gal_lat_deg, gal_lat_min, gal_lat_sec);
   var gal_lon = dms2deg(gal_lon_deg, gal_lon_min, gal_lon_sec);

   // north galactic pole
   const alphaNGP = dms2deg(192, 15, 0);
   const deltaNGP = dms2deg(27, 24, 0);

   // longitude of ascending node of galactic plane
   const lonGP = 33;

   // pre-computed trigonometric functions
   var cdNGP = Math.cos(deg2rad(deltaNGP));
   var sdNGP = Math.sin(deg2rad(deltaNGP));
   var cb = Math.cos(deg2rad(gal_lat));
   var sb = Math.sin(deg2rad(gal_lat));
   var cl = Math.cos(deg2rad(gal_lon - lonGP));
   var sl = Math.sin(deg2rad(gal_lon - lonGP));

   // declination
   var decl = rad2deg(Math.asin(cb*cdNGP*sl + sb*sdNGP));

   // right ascension
   var ra = alphaNGP + rad2deg(Math.atan2(
         cb*cl, sb*cdNGP - cb*sdNGP*sl
      ));
   // convert to sidereal hour, and set in range 0-24
   ra /= 15;
   ra -= 24*Math.floor(ra/24);

   return new Array(ra, decl);
}

// Solve the Kepler equation
//
// Arguments:
// ----------
// M  : the mean anomaly, expressed in radian
// ecc: the eccentricity
//
// Returns:
// --------
// the eccentric anomaly, expressed in radian
function solve_kepler_equation(M, ecc)
{
   var E = M;
   var delta = E - ecc*Math.sin(E) - M;
   while(Math.abs(delta) > 1e-6)
   {
      E -= delta/(1-ecc*Math.cos(E));
      delta = E - ecc*Math.sin(E) - M;
   }
   return E;
}

