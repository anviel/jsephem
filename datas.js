// -----------------------------------------------------------------------------
// File       : datas.js
// Purpose    : datas for the tracking of celestial object
// Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
// Created on : 2021-MAR-22
// Modified on: 2021-MAR-24
// -----------------------------------------------------------------------------

// -------------------------------------
//
// Observation site management
//
// -------------------------------------

// Manage a list of pre-defined sites
function Site(name, lat_deg, lat_min, lat_sec, lon_deg, lon_min, lon_sec)
{
   this.name = name;
   this.lat_deg = lat_deg;
   this.lat_min = lat_min;
   this.lat_sec = lat_sec;
   this.lon_deg = lon_deg;
   this.lon_min = lon_min;
   this.lon_sec = lon_sec;
}

const SITES = new Array(
   new Site("", 0, 0, 0, 0, 0, 0),  // empty dummy site
   new Site("Craponne-sur-Arzon", 45, 19, 55, 3, 50, 56),
   new Site("OBSPM", 48, 50, 11, 2, 20, 11),
   new Site("OHP", 43, 55, 51, 5, 42, 48),
   new Site("OMP", 42, 56, 11, 0, 8, 34),
   new Site("Villerest", 46, 4, 0, 4, 2, 0),
   new Site("AAO Coonabarabran, NSW, Australia", -31, 16, 38, 149, 3, 58),
   new Site("ATCA Mopra, Australia", -31, 16, 10, 149, 5, 54),
   new Site("ATCA Narrabri, Australia", -30, 18, 52, 149, 34, 1),
   new Site("Aachen, Germany", 50, 46, 0, 6, 6, 0),
   new Site("Aberdeen, Scotland", 57, 12, 25, -2, 11, 92),
   new Site("Abilene, Texas", 32, 27, 5, -99, 43, 51),
   new Site("Adelaide SA, Australia", -34, 55, 0, 138, 35, 0),
   new Site("Akron, Ohio", 41, 5, 0, -81, 30, 44),
   new Site("Albany, New York", 42, 39, 1, -73, 45, 1),
   new Site("Albuquerque, New Mexico", 35, 5, 1, -106, 39, 5),
   new Site("Alice Springs, NT, Australia", -23, 48, 30, 133, 54, 3),
   new Site("Allegheny Observatory", 40, 29, 0, -80, 1, 0),
   new Site("Allentown, Pennsylvania", 40, 36, 11, -75, 28, 6),
   new Site("Altoona, Pennsylvania", 40, 30, 55, -78, 24, 3),
   new Site("Amarillo, Texas", 35, 12, 27, -101, 50, 4),
   new Site("Amsterdam, Netherlands", 52, 22, 0, 4, 54, 0),
   new Site("Anchorage, Alaska", 61, 10, 0, -149, 59, 0),
   new Site("Ann Arbor, Michigan", 42, 16, 59, -83, 44, 52),
   new Site("Antwerp, Belgium", 51, 10, 16, 4, 24, 21),
   new Site("Apache Point Observatory, New Mexico", 32, 46, 49, -105, 49, 13),
   new Site("Archenhold-Sternwarte", 52, 29, 0, 13, 29, 0),
   new Site("Arecibo Radiotelescope, Puerto Rico", 18, 21, 14, -66, 45, 19),
   new Site("Argus Radio Telescope (NAAPO)", 40, 0, 9, -83, 2, 41),
   new Site("Arlington, Virginia", 38, 52, 0, -77, 7, 0),
   new Site("Asheville, North Carolina", 35, 35, 42, -82, 33, 26),
   new Site("Ashland, Kentucky", 38, 28, 36, -82, 38, 23),
   new Site("Astronomisches Inst. Univ. Obs.", 48, 14, 0, 16, 20, 0),
   new Site("Astrophys. Research Consortium (ARC)", 32, 46, 49, -105, 49, 13),
   new Site("Athens, Greece", 37, 58, 0, 23, 44, 0),
   new Site("Atlanta, Georgia", 33, 45, 10, -84, 23, 37),
   new Site("Atlantic City, New Jersey", 39, 21, 32, -74, 25, 53),
   new Site("Augusta, Georgia", 33, 28, 20, -81, 58, 0),
   new Site("Augusta, Maine", 44, 18, 53, -69, 46, 29),
   new Site("Austin, Texas", 30, 16, 9, -97, 44, 37),
   new Site("BIMA, Hat Creek, California", 40, 49, 0, -121, 28, 0),
   new Site("Bakersfield, California", 35, 22, 31, -119, 1, 18),
   new Site("Baltimore, Maryland", 39, 17, 36, -76, 36, 45),
   new Site("Bangkok, Thailand", 13, 44, 0, 100, 30, 0),
   new Site("Bangor, Maine", 44, 48, 13, -68, 46, 18),
   new Site("Barcelona, Spain", 41, 23, 0, 2, 9, 0),
   new Site("Barcelona, Venezuela", 10, 8, 6, -64, 10, 7),
   new Site("Barquisimeto, Venezuela", 10, 3, 57, -69, 18, 46),
   new Site("Baton Rouge, Louisiana", 30, 26, 58, -91, 11, 0),
   new Site("Battle Creek, Michigan", 42, 18, 58, -85, 10, 48),
   new Site("Batumi, Georgia", 43, 40, 0, 40, 0, 0),
   new Site("Bay City, Michigan", 43, 36, 4, -83, 53, 15),
   new Site("Beaumont, Texas", 30, 5, 20, -94, 6, 9),
   new Site("Beijing, China", 39, 54, 0, 116, 28, 0),
   new Site("Beirut, Lebanon", 33, 54, 0, 35, 28, 0),
   new Site("Belfast, Northern Ireland", 54, 35, 0, -5, 50, 0),
   new Site("Bellingham, Washington", 48, 45, 34, -122, 28, 36),
   new Site("Bentley WA, Australia", -31, 58, 43, 115, 48, 55),
   new Site("Berkeley, California", 37, 52, 10, -122, 16, 17),
   new Site("Berlin, Germany", 52, 31, 0, 13, 25, 0),
   new Site("Bethlehem, Pennsylvania", 40, 37, 16, -75, 22, 34),
   new Site("Billings, Montana", 45, 47, 0, -108, 30, 4),
   new Site("Biloxi, Mississippi", 30, 23, 48, -88, 53, 0),
   new Site("Binghamton, New York", 42, 6, 3, -75, 54, 47),
   new Site("Birmingham, Alabama", 33, 31, 1, -86, 48, 36),
   new Site("Bisei Spaceguard Center", 34, 40, 20, 133, 32, 40),
   new Site("Bismarck, North Dakota", 46, 48, 23, -100, 47, 17),
   new Site("Bloomington, Illinois", 40, 28, 58, -88, 59, 36),
   new Site("Bogota, Colombia", 4, 38, 0, -74, 5, 0),
   new Site("Boise, Idaho", 43, 37, 7, -116, 11, 58),
   new Site("Bombay, India", 19, 5, 3, 72, 51, 3),
   new Site("Bonn, Germany", 50, 44, 0, 7, 6, 0),
   new Site("Bordeaux, France", 44, 50, 0, 0, 35, 0),
   new Site("Boston, Massachusetts", 42, 21, 24, -71, 3, 25),
   new Site("Bowling Green, Kentucky", 36, 59, 41, -86, 26, 33),
   new Site("Bratislava, Slovak Republic", 48, 10, 0, 17, 7, 0),
   new Site("Brattleboro, Vermont", 42, 51, 6, -72, 33, 48),
   new Site("Bremen, Germany", 53, 4, 33, 8, 48, 26),
   new Site("Brest, France", 48, 23, 0, -4, 29, 0),
   new Site("Bridgeport, Connecticut", 41, 10, 49, -73, 11, 22),
   new Site("Brisbane Qld, Australia", -27, 30, 0, 153, 0, 0),
   new Site("Brockton, Massachusetts", 42, 5, 2, -71, 1, 25),
   new Site("Broken Hill NSW, Australia", -31, 57, 0, 141, 26, 0),
   new Site("Brownsville, Texas", 25, 54, 7, -97, 29, 58),
   new Site("Brussels, Belgium", 50, 50, 20, 4, 38, 12),
   new Site("Bucharest, Romania", 44, 25, 50, 26, 6, 50),
   new Site("Budapest, Hungary", 47, 30, 0, 19, 5, 0),
   new Site("Buenos Aires, Argentina", -34, 34, 0, -58, 3, 0),
   new Site("Buffalo, New York", 42, 52, 52, -78, 52, 21),
   new Site("Burlington, Vermont", 44, 28, 34, -73, 12, 46),
   new Site("Butte, Montana", 46, 1, 6, -112, 32, 11),
   new Site("Buxton, North Carolina", 35, 15, 45, -75, 33, 40),
   new Site("Byurakan Observatory", 40, 20, 0, 44, 18, 0),
   new Site("Cairns Qld , Australia", -16, 55, 0, 145, 46, 0),
   new Site("Cairo, Egypt", 31, 15, 0, 31, 3, 0),
   new Site("Calar Alto Observatory", 37, 13, 0, -2, 32, 0),
   new Site("Cambridge, England", 52, 10, 0, 0, 6, 0),
   new Site("Cambridge, Massachusetts", 42, 22, 1, -71, 6, 22),
   new Site("Camden, New Jersey", 39, 56, 41, -75, 7, 14),
   new Site("Canada-France-Hawaii Tel. (CFHT)", 19, 49, 36, -155, 28, 18),
   new Site("Canberra ACT, Australia", -35, 14, 24, 149, 7, 12),
   new Site("Canton, Ohio", 40, 47, 50, -81, 22, 37),
   new Site("Capetown, South Africa", -33, 56, 0, 18, 22, 15),
   new Site("Caracas, Venezuela", 10, 30, 24, -66, 55, 40),
   new Site("Carmel-by-the-Sea, California", 36, 33, 32, -121, 55, 79),
   new Site("Carson City, Nevada", 39, 10, 0, -119, 46, 0),
   new Site("Casey Observatory, AAT", -66, 12, 0, 110, 21, 0),
   new Site("Catalina Observatory, Mt. Bigelow", 32, 25, 0, -110, 43, 54),
   new Site("Cedar Rapids, Iowa", 41, 58, 1, -91, 39, 53),
   new Site("Central Coast (Erina) NSW, Australia", -33, 26, 0, 151, 23, 0),
   new Site("Central Islip, New York", 40, 47, 24, -73, 12, 0),
   new Site("Cerro Tololo Inter-American Obs.", -30, 9, 54, -70, 48, 54),
   new Site("Champaign, Illinois", 40, 7, 5, -88, 14, 48),
   new Site("Charleston, South Carolina", 32, 46, 35, -79, 55, 53),
   new Site("Charleston, West Virginia", 38, 21, 1, -81, 37, 52),
   new Site("Charlotte, North Carolina", 35, 13, 44, -80, 50, 45),
   new Site("Chattanooga, Tennessee", 35, 2, 41, -85, 18, 32),
   new Site("Cheyenne, Wyoming", 41, 8, 9, -104, 49, 7),
   new Site("Chicago, Illinois", 41, 52, 28, -87, 38, 22),
   new Site("Cincinnatti, Ohio", 39, 6, 7, -84, 30, 35),
   new Site("Cleveland, Ohio", 41, 29, 51, -81, 41, 50),
   new Site("Cologne, Germany", 50, 57, 0, 6, 57, 0),
   new Site("Colorado Springs, Colorado", 38, 50, 7, -104, 49, 16),
   new Site("Columbia, Missouri", 38, 57, 3, -92, 19, 46),
   new Site("Columbia, South Carolina", 34, 0, 2, -81, 2, 0),
   new Site("Columbus Project", 32, 42, 0, -109, 51, 0),
   new Site("Columbus, Georgia", 32, 28, 7, -84, 59, 24),
   new Site("Columbus, Ohio", 39, 57, 47, -83, 0, 17),
   new Site("Concord, New Hampshire", 43, 12, 22, -71, 32, 25),
   new Site("Copenhagen, Denmark", 55, 40, 55, 12, 34, 37),
   new Site("Corpus Christi, Texas", 27, 47, 51, -97, 23, 45),
   new Site("Crete, Greece", 35, 20, 0, 25, 9, 0),
   new Site("Crimean Astrophysical Obs.", 44, 44, 0, 34, 0, 0),
   new Site("Dallas, Texas", 32, 47, 9, -96, 47, 37),
   new Site("Darwin, NT, Australia", -12, 24, 58, 130, 52, 53),
   new Site("Davenport, Iowa", 41, 31, 19, -90, 34, 33),
   new Site("Davis Observatory, AAT", -68, 35, 0, 77, 58, 0),
   new Site("Dayton, Ohio", 39, 45, 32, -84, 11, 43),
   new Site("Daytona Beach, Florida", 29, 12, 44, -81, 1, 10),
   new Site("Decatur, Illinois", 39, 50, 42, -88, 56, 47),
   new Site("Denver, Colorado", 39, 44, 58, -104, 59, 22),
   new Site("Des Moines, Iowa", 41, 35, 14, -93, 37, 0),
   new Site("Detroit, Michigan", 42, 19, 48, -83, 2, 57),
   new Site("Dodge City, Kansas", 37, 45, 17, -100, 1, 9),
   new Site("Dominion Astrophysical Observatory", 48, 31, 18, -123, 25, 0),
   new Site("Dominion Radio Astro. Observatory", 49, 20, 0, -119, 36, 0),
   new Site("Dortmund, Germany", 51, 31, 0, 7, 28, 0),
   new Site("Dublin, Ireland", 53, 18, 56, -6, 14, 21),
   new Site("Dubuque, Iowa", 42, 29, 55, -90, 40, 8),
   new Site("Duluth, Minnesota", 46, 46, 56, -92, 6, 24),
   new Site("Durham, North Carolina", 36, 0, 0, -78, 54, 45),
   new Site("Dusseldorf, Germany", 51, 13, 32, 6, 46, 58),
   new Site("ESO 1-meter Schmidt Telescope", -29, 15, 0, -70, 44, 0),
   new Site("Eau Claire, Wisconsin", 44, 48, 31, -91, 29, 49),
   new Site("Edinburgh, Scotland", 55, 55, 0, -3, 10, 0),
   new Site("Edmonton Alberta, Canada", 53, 33, 47, -113, 36, 48),
   new Site("Effelsberg 100m Radiotelescope", 50, 31, 36, 6, 51, 18),
   new Site("El Manzano, Venezuela", 10, 2, 26, -69, 17, 58),
   new Site("El Paso, Texas", 31, 45, 36, -106, 29, 11),
   new Site("Elizabeth, New Jersey", 40, 39, 43, -74, 12, 59),
   new Site("Enid, Oklahoma", 36, 23, 40, -97, 52, 35),
   new Site("Erie, Pennsylvania", 42, 7, 15, -80, 4, 57),
   new Site("Essen, Germany", 51, 27, 0, 7, 1, 0),
   new Site("Etscorn Campus Observatory", 34, 4, 22, -106, 54, 50),
   new Site("Eugene, Oregon", 44, 3, 16, -123, 5, 30),
   new Site("Eureka, California", 40, 48, 8, -124, 9, 46),
   new Site("European Southern Observatory (NTT)", -29, 15, 24, -70, 43, 48),
   new Site("European Southern Observatory (VLT)", -24, 37, 38, -70, 24, 15),
   new Site("Evansville, Indiana", 37, 58, 20, -87, 34, 21),
   new Site("Fabra-ROA at Montsec, Spain", 42, 3, 6, 0, 43, 46),
   new Site("Fall River, Massachusetts", 41, 42, 6, -71, 9, 18),
   new Site("Fargo, North Dakota", 46, 52, 30, -96, 47, 18),
   new Site("Flagstaff, Arizona", 35, 11, 36, -111, 39, 6),
   new Site("Flint, Michigan", 43, 0, 50, -83, 41, 33),
   new Site("Fort Smith, Arkansas", 35, 23, 10, -94, 25, 36),
   new Site("Fort Wayne, Indiana", 41, 4, 21, -85, 8, 26),
   new Site("Fort Worth, Texas", 32, 44, 55, -97, 19, 44),
   new Site("Frankfurt, Germany", 50, 7, 0, 8, 41, 0),
   new Site("Fremont Peak Observatory, CA, USA", 36, 45, 37, -121, 29, 55),
   new Site("Fresno, California", 36, 44, 12, -119, 47, 11),
   new Site("Gadsden, Alabama", 34, 0, 57, -86, 0, 41),
   new Site("Gainesville, Florida", 29, 38, 56, -82, 19, 19),
   new Site("Gallup, New Mexico", 35, 31, 30, -108, 44, 30),
   new Site("Galveston, Texas", 29, 18, 10, -94, 47, 43),
   new Site("Gary, Indiana", 41, 36, 12, -87, 20, 19),
   new Site("Gemini Telescope (north)", 19, 49, 0, -155, 28, 0),
   new Site("Gemini Telescope (south)", -30, 21, 0, -70, 49, 0),
   new Site("Geneva, Switzerland", 46, 14, 0, 6, 6, 0),
   new Site("Gillam Manitoba, Canada", 56, 22, 48, -94, 38, 24),
   new Site("Grand Junction, Colorado", 39, 4, 6, -108, 33, 54),
   new Site("Grand Rapids, Michigan", 42, 58, 3, -85, 40, 13),
   new Site("Graz, Austria", 47, 4, 1, 15, 27, 0),
   new Site("Great Falls, Montana", 47, 29, 33, -111, 18, 23),
   new Site("Green Bay, Wisconsin", 44, 30, 48, -88, 0, 50),
   new Site("Greensboro, North Carolina", 36, 4, 17, -79, 47, 25),
   new Site("Greenville, South Carolina", 34, 50, 50, -82, 24, 1),
   new Site("Greenwich, England", 51, 30, 0, 0, 0, 0),
   new Site("Groton, New York", 42, 35, 26, -72, 33, 20),
   new Site("Guatemala City, Guatemala", 14, 38, 0, -90, 22, 0),
   new Site("Gulfport, Mississippi", 30, 22, 4, -89, 5, 36),
   new Site("Halifax Nova Scotia, Canada", 44, 38, 55, -63, 34, 25),
   new Site("Hamburg, Germany", 53, 33, 0, 9, 59, 0),
   new Site("Hamilton, Ohio", 39, 23, 59, -84, 33, 47),
   new Site("Hanging Rock, Victoria, Australia", -37, 19, 81, 144, 35, 66),
   new Site("Hannover, Germany", 52, 22, 28, 9, 44, 19),
   new Site("Harare, Zimbabwe", -17, 50, 0, 31, 3, 0),
   new Site("Harrisburg, Pennsylvania", 40, 15, 43, -76, 52, 59),
   new Site("Hartford, Connecticut", 41, 46, 12, -72, 40, 49),
   new Site("Helena, Montana", 46, 35, 33, -112, 2, 24),
   new Site("Helsinki, Finland", 60, 10, 0, 24, 50, 0),
   new Site("Hobart Tas, Australia", -42, 50, 0, 147, 20, 0),
   new Site("Holyoke, Massachusetts", 42, 12, 29, -72, 36, 36),
   new Site("Honolulu, Hawaii", 21, 18, 22, -157, 51, 35),
   new Site("Houston, Texas", 29, 45, 26, -95, 21, 37),
   new Site("Huntington, West Virginia", 38, 25, 12, -82, 26, 33),
   new Site("Huntsville, Alabama", 34, 44, 18, -86, 35, 19),
   new Site("Ilford NSW, Australia", -32, 58, 0, 149, 52, 0),
   new Site("Indianapolis, Indiana", 39, 46, 7, -86, 9, 46),
   new Site("Iowa City, Iowa", 41, 39, 37, -91, 31, 53),
   new Site("Isaac Newton Telescope (98-inch)", 28, 46, 0, -17, 53, 0),
   new Site("Jackson, Michigan", 42, 14, 43, -84, 24, 22),
   new Site("Jackson, Mississippi", 32, 17, 56, -90, 11, 6),
   new Site("Jacksonville, Florida", 30, 19, 44, -81, 39, 42),
   new Site("Jakarta, Indonesia", -6, 11, 0, 106, 50, 0),
   new Site("Jersey City, New Jersey", 40, 43, 50, -74, 3, 56),
   new Site("Jerusalem, Israel", 31, 47, 0, 35, 13, 0),
   new Site("Johannesburg, South Africa", -26, 10, 0, 28, 2, 0),
   new Site("Johnstown, Pennsylvania", 40, 19, 35, -78, 55, 3),
   new Site("Joplin, Missouri", 37, 5, 26, -94, 30, 0),
   new Site("Juneau, Alaska", 58, 18, 12, -134, 24, 30),
   new Site("KHBI, Saipan, MI", 15, 7, 20, 145, 41, 37),
   new Site("Kalamazoo, Michigan", 42, 17, 29, -85, 35, 14),
   new Site("Kansas City, Kansas", 39, 7, 4, -94, 38, 24),
   new Site("Kansas City, Missouri", 39, 7, 56, -94, 35, 20),
   new Site("Karl Schwarzschild Observatorium", 50, 59, 0, 11, 43, 0),
   new Site("Kenosha, Wisconsin", 42, 35, 43, -87, 50, 11),
   new Site("Kensington NSW, Australia", -33, 52, 0, 151, 13, 0),
   new Site("Key West, Florida", 24, 33, 30, -81, 48, 12),
   new Site("Kiso Observatory", 35, 48, 0, 137, 38, 0),
   new Site("Kitt Peak National Obs.", 31, 58, 48, -111, 36, 0),
   new Site("Knoxville, Tennessee", 35, 57, 39, -83, 55, 7),
   new Site("Kuala Lumpur, Malaysia", 3, 8, 0, 101, 42, 0),
   new Site("Lafayette, Indiana", 40, 25, 11, -86, 53, 39),
   new Site("Lancaster, Pennsylvania", 40, 2, 25, -76, 18, 29),
   new Site("Lansing, Michigan", 42, 44, 1, -84, 33, 15),
   new Site("Laredo, Texas", 27, 30, 22, -99, 30, 30),
   new Site("Large Binocular Telescope", 32, 42, 5, -109, 53, 21),
   new Site("Large Earth-based Solar Tel. (LEST)", 28, 46, 0, -17, 53, 0),
   new Site("Las Campanas Observatory", -29, 0, 12, -70, 42, 6),
   new Site("Las Vegas, Nevada", 36, 10, 20, -115, 8, 37),
   new Site("Lawrence, Massachusetts", 42, 42, 16, -71, 10, 8),
   new Site("Leander McCormick Observatory", 38, 2, 0, -78, 31, 0),
   new Site("Learmonth Solar Observatory", -22, 14, 0, 114, 5, 0),
   new Site("Lexington, Kentucky", 38, 2, 50, -84, 29, 46),
   new Site("Lick Observatory", 37, 21, 0, -121, 38, 0),
   new Site("Lille, France", 50, 38, 0, 3, 4, 0),
   new Site("Lima, Ohio", 40, 44, 35, -84, 6, 20),
   new Site("Lincoln, Nebraska", 40, 48, 59, -96, 42, 15),
   new Site("Lisbon, Portugal", 38, 43, 0, -9, 8, 0),
   new Site("Little Rock, Arkansas", 34, 44, 42, -92, 16, 37),
   new Site("London, England", 51, 30, 30, 0, 11, 0),
   new Site("Long Beach, California", 33, 46, 14, -118, 11, 18),
   new Site("Longyearbyen, Norway", 78, 12, 9, 15, 49, 44),
   new Site("Lorain, Ohio", 41, 28, 5, -82, 10, 49),
   new Site("Los Angeles, California", 34, 3, 15, -118, 14, 28),
   new Site("Louisville, Kentucky", 38, 14, 47, -85, 45, 49),
   new Site("Lowell, Massachusetts", 42, 38, 25, -71, 19, 14),
   new Site("Lubbock, Texas", 33, 35, 5, -101, 50, 33),
   new Site("Lund, Sweden", 55, 69, 0, 13, 18, 0),
   new Site("Luxembourg, Luxembourg", 49, 37, 0, 6, 9, 0),
   new Site("MMT", 31, 41, 0, -110, 53, 0),
   new Site("Macon, Georgia", 32, 50, 12, -83, 37, 36),
   new Site("Macquarie Island, AAT", -54, 30, 0, 158, 57, 0),
   new Site("Madison, Wisconsin", 43, 4, 23, -89, 22, 55),
   new Site("Madrid, Spain", 40, 25, 0, -3, 42, 0),
   new Site("Magdalena Ridge Obs, NM, USA", 33, 59, 6, -107, 11, 21),
   new Site("Managua, Nicaragua", 13, 16, 0, -87, 40, 0),
   new Site("Manchester, New Hampshire", 42, 59, 26, -71, 27, 41),
   new Site("Marseille, France", 43, 17, 0, 5, 22, 0),
   new Site("Marshall, Texas", 32, 33, 0, -94, 23, 0),
   new Site("Mauna Kea Observatory (NASA IRTF)", 19, 50, 0, -155, 28, 0),
   new Site("Mawson Observatory, AAT", -67, 36, 0, 62, 53, 0),
   new Site("McDonald Observatory", 30, 40, 18, -104, 1, 18),
   new Site("Melbourne Vic, Australia", -37, 48, 0, 144, 58, 0),
   new Site("Memphis, Tennessee", 35, 8, 46, -90, 3, 13),
   new Site("Menville, France", 43, 40, 41, 1, 11, 40),
   new Site("Merida, Venezuela", 8, 35, 56, -71, 9, 12),
   new Site("Meriden, Connecticut", 41, 32, 6, -72, 47, 30),
   new Site("Metepec, Puebla, Mexico", 18, 55, 93, -98, 28, 1),
   new Site("Miami, Florida", 25, 46, 37, -80, 11, 32),
   new Site("Michigan-Dartmouth-MIT Obs.", 31, 57, 0, -111, 37, 0),
   new Site("Milano, Italy", 45, 26, 35, 9, 11, 9),
   new Site("Milwaukee, Wisconsin", 43, 2, 19, -87, 54, 15),
   new Site("Minneapolis, Minnesota", 44, 58, 57, -93, 15, 43),
   new Site("Minot, North Dakota", 48, 14, 9, -101, 17, 38),
   new Site("Mobile, Alabama", 30, 41, 36, -88, 2, 33),
   new Site("Moline, Illinois", 41, 30, 31, -90, 30, 49),
   new Site("Monte Alban, Oaxaca, Mexico", 17, 2, 67, -96, 46, 13),
   new Site("Monterey, California", 36, 37, 46, -121, 50, 50),
   new Site("Montgomery, Alabama", 32, 22, 33, -86, 18, 31),
   new Site("Montpelier, Vermont", 44, 15, 36, -72, 34, 41),
   new Site("Montreal Quebec, Canada", 45, 30, 24, -73, 34, 2),
   new Site("Moratuwa, Sri Lanka", 6, 45, 0, 79, 55, 0),
   new Site("Moscow, Russia", 55, 45, 0, 37, 42, 0),
   new Site("Mount Wilson Observatory", 34, 13, 0, -118, 3, 0),
   new Site("Mt. Stromlo Observatory", -35, 19, 15, 149, 1, 28),
   new Site("Mudgee NSW, Australia", -32, 36, 0, 150, 52, 12),
   new Site("Muncie, Indiana", 40, 11, 28, -85, 23, 16),
   new Site("Munich, Germany", 48, 8, 0, 11, 34, 0),
   new Site("Nancay Radiotelescope, France", 47, 22, 48, 2, 11, 48),
   new Site("Naples, Italy", 40, 51, 46, 14, 15, 18),
   new Site("Nashville, Tennessee", 36, 9, 33, -86, 46, 55),
   new Site("Natchez, Mississippi", 31, 33, 48, -91, 23, 30),
   new Site("National Astronomy Obs. (Japan)", 19, 49, 0, -155, 28, 0),
   new Site("New Bedford, Massachusetts", 41, 38, 13, -70, 55, 41),
   new Site("New Britain, Connecticut", 41, 40, 8, -72, 46, 59),
   new Site("New Delhi, India", 28, 38, 0, 77, 12, 0),
   new Site("New Haven, Connecticut", 41, 18, 25, -72, 55, 30),
   new Site("New Orleans, Louisiana", 29, 56, 53, -90, 4, 10),
   new Site("New York, New York", 40, 45, 6, -73, 59, 39),
   new Site("Newark, New Jersey", 40, 44, 14, -74, 10, 19),
   new Site("Newcastle NSW, Australia", -32, 52, 0, 151, 49, 0),
   new Site("Niagara Falls, New York", 43, 5, 34, -79, 3, 26),
   new Site("Nordic Optical Telescope", 28, 45, 0, -17, 53, 0),
   new Site("Norfolk, Virginia", 36, 51, 10, -76, 17, 21),
   new Site("North Liberty Radio Observatory", 41, 46, 17, -91, 34, 26),
   new Site("OAB, Brera  (MI), Italy", 45, 27, 59, 9, 11, 30),
   new Site("OAB, Merate (LC), Italy", 45, 41, 54, 9, 25, 45),
   new Site("Oakland, California", 37, 48, 3, -122, 15, 54),
   new Site("Oaxaca, Oaxaca, Mexico", 17, 3, 45, -96, 43, 85),
   new Site("Obs. Astro. Nacional, San Pedro Martir", 31, 1, 45, -115, 29, 12),
   new Site("Obs. Astro. Nacional, Tonantzintla", 19, 1, 58, -98, 18, 50),
   new Site("Observatoire Royal de Belgique", 50, 48, 0, 4, 21, 0),
   new Site("Observatoire de Besancon", 47, 14, 50, 5, 59, 23),
   new Site("Observatoire de Calern", 43, 45, 18, 6, 56, 18),
   new Site("Observatoire de Nice", 43, 43, 0, 7, 18, 0),
   new Site("Observatoire de Paris", 48, 48, 0, 2, 14, 0),
   new Site("Ogden, Utah", 41, 13, 31, -111, 58, 21),
   new Site("Oklahoma City, Oklahoma", 35, 28, 26, -97, 31, 4),
   new Site("Old Royal Observatory", 51, 29, 0, 0, 0, 0),
   new Site("Omaha, Nebraska", 41, 15, 42, -95, 56, 14),
   new Site("Orlando, Florida", 28, 32, 42, -81, 22, 38),
   new Site("Oschin 48-inch Telescope", 33, 21, 0, -116, 51, 0),
   new Site("Oslo, Norway", 59, 55, 0, 10, 45, 0),
   new Site("Ottawa, Ontario, Canada", 45, 24, 0, -75, 39, 0),
   new Site("Paducah, Kentucky", 37, 5, 13, -88, 25, 56),
   new Site("Palomar Observatory (200 inch)", 33, 21, 0, -116, 52, 0),
   new Site("Paris, France", 48, 50, 14, 2, 20, 14),
   new Site("Parkes Radiotelescope, Australia", -33, 0, 0, 148, 15, 42),
   new Site("Pasadena, California", 34, 8, 44, -118, 8, 41),
   new Site("Paterson, New Jersey", 40, 55, 1, -74, 10, 21),
   new Site("Pensacola, Florida", 30, 24, 51, -87, 12, 56),
   new Site("Peoria, Illinois", 40, 41, 42, -89, 35, 33),
   new Site("Perth, WA, Australia", -31, 56, 29, 115, 57, 55),
   new Site("Philadelphia, Pennsylvania", 39, 56, 58, -75, 9, 21),
   new Site("Phoenix, Arizona", 33, 27, 12, -112, 4, 28),
   new Site("Pierre, South Dakota", 44, 22, 18, -100, 20, 54),
   new Site("Pittsburgh, Pennsylvania", 40, 29, 19, -80, 0, 0),
   new Site("Pittsfield, Massachusetts", 42, 26, 53, -73, 15, 14),
   new Site("Pocatello, Idaho", 42, 51, 38, -112, 27, 1),
   new Site("Port Arthur, Texas", 29, 52, 30, -93, 56, 15),
   new Site("Portland, Maine", 43, 39, 33, -70, 15, 19),
   new Site("Portland, Oregon", 45, 31, 6, -122, 40, 35),
   new Site("Portsmouth, New Hampshire", 43, 4, 30, -70, 45, 24),
   new Site("Portsmouth, Virginia", 36, 50, 7, -76, 18, 14),
   new Site("Prague, Czech Republic", 50, 5, 0, 14, 24, 0),
   new Site("Pretoria, South Africa", -26, 10, 0, 28, 2, 0),
   new Site("Providence, Rhode Island", 41, 49, 32, -71, 24, 41),
   new Site("Provo, Utah", 40, 14, 6, -111, 39, 24),
   new Site("Prude Ranch, Fort Davis, TX", 30, 36, 38, -103, 57, 7),
   new Site("Puebla, Puebla, Mexico", 19, 2, 43, -98, 11, 83),
   new Site("Pueblo, Colorado", 38, 16, 17, -104, 36, 33),
   new Site("Racine, Wisconsin", 42, 43, 49, -87, 47, 12),
   new Site("Radioastrophysical Observatory", 56, 47, 0, 24, 24, 0),
   new Site("Rainwater Observatory, French Camp, MS", 33, 17, 15, -89, 23, 5),
   new Site("Raleigh, North Carolina", 35, 46, 38, -78, 38, 21),
   new Site("Rankin Inlet Nunavut, Canada", 62, 49, 12, -92, 6, 36),
   new Site("Rapid City, South Dakota", 44, 4, 52, -103, 13, 11),
   new Site("Reading, Pennsylvania", 40, 20, 9, -75, 55, 40),
   new Site("Real Observario de la Armada, Spain", 36, 27, 52, -6, 12, 18),
   new Site("Reno, Nevada", 39, 31, 27, -119, 48, 40),
   new Site("Reykjavik, Iceland", 64, 7, 47, -21, 56, 13),
   new Site("Richmond, Virginia", 37, 32, 15, -77, 26, 9),
   new Site("Rio de Janeiro, Brazil", -22, 53, 43, -43, 13, 22),
   new Site("Roanoke, Virginia", 37, 16, 13, -79, 56, 44),
   new Site("Rochester, Minnesota", 44, 1, 21, -92, 28, 3),
   new Site("Rochester, New York", 43, 9, 41, -77, 36, 21),
   new Site("Rockford, Illinois", 42, 16, 7, -89, 5, 48),
   new Site("Rome, Italy", 41, 53, 0, 12, 30, 0),
   new Site("Roque de los Muchachos, La Palma", 28, 45, 30, -17, 52, 48),
   new Site("Royal Greenwich Observatory", 50, 52, 0, 20, 0, 0),
   new Site("Sacramento, California", 38, 34, 57, -121, 29, 41),
   new Site("Saginaw, Michigan", 43, 25, 52, -83, 56, 5),
   new Site("Saint Cloud, Minnesota", 45, 34, 0, -94, 10, 24),
   new Site("Saint Joseph, Missouri", 39, 45, 57, -94, 51, 2),
   new Site("Saint Louis, Missouri", 38, 37, 45, -90, 12, 22),
   new Site("Saint Lucia Qld, Australia", -27, 30, 2, 153, 0, 41),
   new Site("Saint Paul, Minnesota", 44, 57, 19, -93, 6, 7),
   new Site("Saint Petersburg, Florida", 27, 46, 18, -82, 38, 19),
   new Site("Salem, Oregon", 44, 56, 24, -123, 1, 59),
   new Site("Salina, Kansas", 38, 50, 36, -97, 36, 46),
   new Site("Salt Lake City, Utah", 40, 45, 23, -111, 53, 26),
   new Site("Salzburg, Austria", 47, 47, 44, 13, 3, 16),
   new Site("San Angelo, Texas", 31, 27, 39, -100, 26, 3),
   new Site("San Antonio, Texas", 29, 25, 37, -98, 29, 6),
   new Site("San Bernardino, California", 34, 6, 30, -117, 17, 28),
   new Site("San Diego, California", 32, 42, 53, -117, 9, 21),
   new Site("San Francisco, California", 37, 46, 39, -122, 24, 40),
   new Site("San Jose, California", 37, 20, 16, -121, 53, 24),
   new Site("San Juan, Puerto Rico", 18, 28, 0, -66, 7, 0),
   new Site("Santa Barbara, California", 34, 25, 18, -119, 41, 55),
   new Site("Santa Cruz, California", 36, 58, 18, -122, 1, 18),
   new Site("Santa Fe, New Mexico", 35, 41, 11, -105, 56, 10),
   new Site("Santiago, Chile", -33, 27, 0, -70, 33, 0),
   new Site("Santo Domingo, Dominican Republic", 18, 30, 0, -69, 57, 0),
   new Site("Sapporo, Japan", 43, 4, 0, 141, 20, 0),
   new Site("Sarasota, Florida", 27, 20, 5, -82, 32, 30),
   new Site("Savannah, Georgia", 32, 4, 42, -81, 5, 37),
   new Site("Schenectady, New York", 42, 48, 42, -73, 55, 42),
   new Site("Scranton, Pennsylvania", 41, 24, 32, -75, 39, 46),
   new Site("Seattle, Washington", 47, 36, 32, -122, 20, 12),
   new Site("Sheboygan, Wisconsin", 43, 45, 3, -87, 42, 52),
   new Site("Sheridan, Wyoming", 44, 47, 55, -106, 57, 10),
   new Site("Shreveport, Louisiana", 32, 30, 46, -93, 44, 58),
   new Site("Siding Spring Observatory", -31, 16, 24, 149, 3, 41),
   new Site("Singapore, Singapore", 1, 25, 0, 103, 50, 0),
   new Site("Sioux City, Iowa", 42, 29, 46, -96, 24, 30),
   new Site("Sioux Falls, South Dakota", 43, 32, 35, -96, 43, 35),
   new Site("Socorro, NM", 34, 3, 11, -106, 53, 28),
   new Site("Sofia, Bulgaria", 42, 40, 0, 23, 18, 0),
   new Site("Somerville, Massachusetts", 42, 23, 15, -71, 6, 7),
   new Site("South Bend, Indiana", 41, 40, 33, -86, 15, 1),
   new Site("Space Telescope Science Inst.", 39, 18, 0, -76, 37, 0),
   new Site("Spartanburg, South Carolina", 34, 57, 3, -81, 56, 6),
   new Site("Special Astrophysical Obs.", 43, 39, 0, 41, 26, 0),
   new Site("Spokane, Washington", 47, 39, 32, -117, 25, 33),
   new Site("Springfield, Illinois", 39, 47, 58, -89, 38, 51),
   new Site("Springfield, Massachusetts", 42, 6, 21, -72, 35, 32),
   new Site("Springfield, Missouri", 37, 13, 3, -93, 17, 32),
   new Site("Springfield, Ohio", 39, 55, 38, -83, 48, 29),
   new Site("St. Petersburg, Russia", 59, 55, 0, 30, 15, 0),
   new Site("Stamford, Connecticut", 41, 3, 9, -73, 32, 24),
   new Site("Steubenville, Ohio", 40, 21, 42, -80, 36, 53),
   new Site("Stockholm, Sweden", 59, 35, 0, 18, 6, 0),
   new Site("Stockton, California", 37, 57, 30, -121, 17, 16),
   new Site("Strasbourg, France", 48, 35, 0, 7, 45, 0),
   new Site("Stuttgart, Germany", 48, 47, 0, 9, 11, 0),
   new Site("Superior, Wisconsin", 46, 43, 14, -92, 6, 7),
   new Site("Surfers Paradise Qld, Australia", -28, 0, 2, 153, 26, 0),
   new Site("Sydney NSW, Australia", -33, 52, 0, 151, 12, 0),
   new Site("Syracuse, New York", 43, 3, 4, -76, 9, 14),
   new Site("Tacoma, Washington", 47, 14, 59, -122, 26, 15),
   new Site("Taipei, Taiwan", 25, 2, 0, 121, 31, 0),
   new Site("Tallahassee, Florida", 30, 26, 30, -84, 16, 56),
   new Site("Tallinn, Estonia", 59, 27, 0, 24, 45, 0),
   new Site("Tampa, Florida", 27, 56, 58, -82, 27, 25),
   new Site("Telescopio Nazionale Galileo", 28, 46, 0, -17, 53, 0),
   new Site("Terre Haute, Indiana", 39, 28, 3, -87, 24, 26),
   new Site("Texarkana, Texas", 33, 25, 48, -94, 2, 30),
   new Site("Tokyo, Japan", 35, 45, 0, 139, 45, 0),
   new Site("Toledo, Ohio", 41, 39, 14, -83, 32, 39),
   new Site("Topeka, Kansas", 39, 3, 16, -95, 40, 23),
   new Site("Toronto Ontario, Canada", 43, 39, 10, -79, 23, 0),
   new Site("Torun Radiotelescope, Poland", 52, 54, 38, 18, 33, 50),
   new Site("Torus Observatory", 47, 10, 58, -122, 7, 57),
   new Site("Trenton, New Jersey", 40, 13, 14, -74, 46, 13),
   new Site("Tromso, Norway", 69, 41, 0, 18, 95, 0),
   new Site("Troy, New York", 42, 43, 45, -73, 40, 58),
   new Site("Tucson, Arizona", 32, 13, 15, -110, 58, 8),
   new Site("Tulsa, Oklahoma", 36, 9, 12, -95, 59, 34),
   new Site("U. S. Naval Observatory", 38, 55, 0, -77, 4, 0),
   new Site("United Kingdom Infrared Tel. (UKIRT)", 19, 50, 0, -155, 28, 0),
   new Site("United Kingdom Schmidt Tel. Unit", -31, 16, 0, 149, 4, 0),
   new Site("Univ. of BC and Laval Univ.", 49, 7, 0, -122, 35, 0),
   new Site("Uppsala University Observatory", 59, 30, 0, 17, 36, 0),
   new Site("Urbana, Illinois", 40, 6, 42, -88, 12, 6),
   new Site("Utica, New York", 43, 6, 12, -75, 13, 33),
   new Site("Vatican Advanced Technology Telescope", 32, 42, 5, -109, 53, 33),
   new Site("VLA, New Mexico", 34, 4, 44, -107, 37, 4),
   new Site("VLBA, Brewster, WA", 48, 7, 52, -119, 40, 55),
   new Site("VLBA, Fort Davis, TX", 30, 38, 5, -103, 56, 39),
   new Site("VLBA, Hancock, NH", 42, 56, 0, -71, 59, 11),
   new Site("VLBA, Kitt Peak, AZ", 31, 57, 22, -111, 36, 42),
   new Site("VLBA, Los Alamos, NM", 35, 46, 30, -106, 14, 42),
   new Site("VLBA, Mauna Kea, HI", 19, 48, 15, -155, 27, 28),
   new Site("VLBA, N. Liberty, IA", 41, 46, 17, -91, 34, 26),
   new Site("VLBA, Owens Vly., CA", 37, 13, 54, -118, 16, 33),
   new Site("VLBA, Pie Town, NM", 34, 18, 3, -108, 7, 7),
   new Site("VLBA, St. Croix, VI", 17, 45, 30, -64, 35, 2),
   new Site("Vainu Bappu Observatory", 12, 35, 0, 78, 50, 0),
   new Site("Vancouver British Columbia, Canada", 49, 16, 32, -123, 8, 31),
   new Site("Varna, Bulgaria", 43, 12, 0, 27, 57, 0),
   new Site("Venezuela 1-meter Schmidt", 8, 47, 0, -70, 52, 0),
   new Site("Victoria British Columbia, Canada", 48, 30, 0, -123, 25, 0),
   new Site("Vienna, Austria", 48, 12, 0, 16, 22, 0),
   new Site("W. M. Keck Observatory", 19, 49, 0, -155, 28, 0),
   new Site("WIYN Observatory", 31, 57, 0, -111, 36, 0),
   new Site("WSHB, Cyprus Creek, South Carolina", 32, 41, 2, -81, 7, 51),
   new Site("Waco, Texas", 31, 33, 12, -97, 8, 0),
   new Site("Walla Walla, Washington", 46, 4, 8, -118, 20, 24),
   new Site("Warsaw, Poland", 52, 15, 0, 21, 0, 0),
   new Site("Washington, DC", 38, 53, 51, -77, 0, 33),
   new Site("Waterbury, Connecticut", 41, 33, 13, -73, 2, 31),
   new Site("Waterloo, Iowa", 42, 29, 40, -92, 20, 20),
   new Site("Wellington, New Zealand", -41, 17, 25, 174, 46, 7),
   new Site("West Palm Beach, Florida", 26, 42, 36, -80, 3, 7),
   new Site("Wheeling, West Virginia", 40, 4, 3, -80, 43, 20),
   new Site("White Plains, New York", 41, 2, 0, -73, 45, 48),
   new Site("Wichita Falls, Texas", 33, 54, 34, -98, 29, 28),
   new Site("Wichita, Kansas", 37, 41, 30, -97, 20, 16),
   new Site("Wilkes-Barre, Pennsylvania", 41, 14, 32, -75, 53, 17),
   new Site("William Herschel Telescope", 28, 46, 0, -17, 53, 0),
   new Site("Wilmington, Delaware", 39, 44, 46, -75, 32, 51),
   new Site("Wilmington, North Carolina", 34, 14, 14, -77, 56, 58),
   new Site("Winer Mobile Observatory", 31, 39, 56, -110, 36, 6),
   new Site("Winnipeg Manitoba, Canada", 49, 50, 0, -97, 7, 0),
   new Site("Winston-Salem, North Carolina", 36, 5, 52, -80, 14, 42),
   new Site("Wodonga Vic, Australia", -36, 10, 0, 146, 50, 0),
   new Site("Wollongong NSW, Australia", -34, 24, 0, 150, 54, 0),
   new Site("Worcester, Massachusetts", 42, 15, 37, -71, 48, 17),
   new Site("Wyoming Infrared Observatory", 41, 5, 54, -105, 58, 36),
   new Site("Yakima, Washington", 46, 36, 9, -120, 30, 39),
   new Site("Yerkes Observatory", 42, 34, 0, -88, 33, 0),
   new Site("Yonkers, New York", 40, 55, 55, -73, 53, 54),
   new Site("York, Pennsylvania", 39, 57, 35, -76, 43, 36),
   new Site("Youngstown, Ohio", 41, 5, 57, -80, 39, 2),
   new Site("Yuma, Arizona", 32, 42, 54, -114, 37, 24),
   new Site("Zagreb, Croatia", 45, 48, 43, 15, 58, 52),
   new Site("Zanesville, Ohio", 39, 56, 18, -82, 0, 30),
   new Site("Zentralinstitut fur Astrophysik", 52, 23, 0, 13, 4, 0),
   new Site("Zurich, Switzerland", 47, 22, 40, 8, 33, 4)
);

function populate_site_select(site_select)
{
   site_select.options.length = 0;
   for(var i = 0; i < SITES.length; i++)
   {
      if (SITES[i].name == "Villerest")
         site_select.options[i] = new Option(SITES[i].name, i, true, true);
      else
         site_select.options[i] = new Option(SITES[i].name, i, false, false);
   }
}

// -------------------------------------
//
// Planets as special celestial objects
//
// -------------------------------------
function Planet(name_en, name_fr, position)
{
   // special case for french language
   if (document.documentElement.lang.substring(0,2) == "fr")
   {
      this.name = name_fr;
   }
   else
   {
      this.name = name_en;
   }
   this.position = position;
}
// Low precision ephemeris for the Sun. Accurate to 0.01 degree.
// The latitude is presumed to be zero.
function sun_position(d)
{
   const _kL0 = new Array(280.46646, 36000.76983, 0.0003032);
   const _kM = new Array(357.5291092, 35999.0502909, -0.0001536, 1.0/24490000);
   const _kC = new Array(1.914602, -0.004817, -0.000014);
   const _ck3 = 0.019993;
   const _ck4 = -0.000101;
   const _ck5 = 0.000289;

   // compute ecliptic longitude, expressed in degree
   var jd = date2jd(d);
   var T = jd2jcent(jd);
   var L0 = polynomial(_kL0, T);
   var M = deg2rad(polynomial(_kM, T));
   var C = polynomial(_kC, T) * Math.sin(M)
        + (_ck3 - _ck4 * T) * Math.sin(2 * M)
        + _ck5 * Math.sin(3 * M);
   var L = L0+C;
   L = mod360(L);

   return ecliptic2equatorial(jd, 0, L);
}
// Compute the orbital position of an object in rectangular coordinates,
// usually the heliocentric ecliptic rectangular coordinates of the planet
// which orbital elements are given as argument, for Julian day number jd
//
// Arguments:
// ----------
// jd        : the Julian day number at which the position is desired
// jd_epoch  : the Julian day number of the epoch
//             for the following orbital elements
// sid_period: the sidereal period, expressed in day
// mean_lon  : the mean longitude at epoch, expressed in degree
// peri_lon  : the longitude of periastron, expressed in degree
// ecc       : the eccentricity
// sma       : the semi major axis, in arbitrary unit
// incli     : the inclination, expressed in degree
// ascn_lon  : the longitude of the ascending node, expressed in degree
//
// Returns:
// --------
// a 3-elements vector, the rectangular coordinates of object,
// expressed in the same unit as sma
function orbital_position(jd, jd_epoch, sid_period,
                          mean_lon, peri_lon, ecc, sma, incli, ascn_lon)
{
   // mean anomaly, expressed in radian
   var mean_anom = 2*Math.PI*(jd-jd_epoch)/sid_period
                   + deg2rad(mean_lon - peri_lon);
   // eccentric anomaly, expressed in radian
   var ecc_anom = solve_kepler_equation(mean_anom, ecc);
   // heliocentric orbital plane rectangular coordinates,
   // expressed in the same unit as sma
   var p0 = new Array(
         sma*(Math.cos(ecc_anom)-ecc),
         sma*Math.sqrt(1-ecc*ecc)*Math.sin(ecc_anom),    
         0
      );
   // argument of perihelion, expressed in radian
   var peri_arg = deg2rad(peri_lon - ascn_lon);
   var cpa = Math.cos(peri_arg);
   var spa = Math.sin(peri_arg);
   // coordinate system change from orbital plane to heliocentric ecliptic
   var p1 = column_matrix_dot_vector(
         new Array(  cpa, spa, 0 ),
         new Array( -spa, cpa, 0 ),
         new Array(    0,   0, 1 ),
         p0
      );
   var ci = Math.cos(deg2rad(incli));
   var si = Math.sin(deg2rad(incli));
   var p2 = column_matrix_dot_vector(
         new Array( 1,   0,  0 ),
         new Array( 0,  ci, si ),
         new Array( 0, -si, ci ),
         p1
      );
   var ca = Math.cos(deg2rad(ascn_lon));
   var sa = Math.sin(deg2rad(ascn_lon));
   var p3 = column_matrix_dot_vector(
         new Array(  ca, sa, 0 ),
         new Array( -sa, ca, 0 ),
         new Array(   0,  0, 1 ),
         p2
      );
   // return the rectangular coordinates as 3-elements vector,
   // expressed in the unit of sma
   return p3;
}
function planet_position(d, epoch, sid_period,
                         mean_lon, peri_lon, ecc, sma, incli, ascn_lon)
{
   // Earth year definitions, expressed in days
   const tropical_year = 365.242191;
   const sidereal_year = 365.2564;    
   // Julian day number
   var jd = date2jd(d);
   // get Julian day number of epoch (= first day of year)
   var d0 = new Date();
   d0.setUTCFullYear(epoch); d0.setUTCMonth(0); d0.setUTCDate(0);
   d0.setUTCHours(0); d0.setUTCMinutes(0); d0.setUTCSeconds(0);
   var jd_epoch = date2jd(d0);
   // geocentric ecliptic rectangular coordinates
   var gerc = vector_minus_vector(
      orbital_position(jd, jd_epoch, sid_period*tropical_year, mean_lon, peri_lon, ecc, sma, incli, ascn_lon),
      orbital_position(jd, jd_epoch, sidereal_year, 99.403308, 102.768413, 0.016713, 1, 0, 0) // earth
   );
   // geocentric ecliptic spherical coordinates
   var ecl_lat = rad2deg(Math.asin(gerc[2]/norm_vect(gerc)));
   var ecl_lon = rad2deg(Math.atan2(gerc[1], gerc[0]));
   return ecliptic2equatorial(jd, ecl_lat, ecl_lon);
}
function mercury_position(d)
{
   return planet_position(d, 1990, 0.240852, 60.750646, 77.299833, 0.205633, 0.387099, 7.004540, 48.212740);
}
function venus_position(d)
{
   return planet_position(d, 1990, 0.615211, 88.455855, 131.430236, 0.006778, 0.723332, 3.394535, 76.589820);
}
function mars_position(d)
{
   return planet_position(d, 1990, 1.880932, 240.739474, 335.874939, 0.093396, 1.523688, 1.849736, 49.480308);
}
function jupiter_position(d)
{
   return planet_position(d, 1990, 11.863075, 90.638185, 14.170747, 0.048482, 5.202561, 1.303613, 100.353142);
}
function saturn_position(d)
{
   return planet_position(d, 1990, 29.471362, 287.690033, 92.861407, 0.055581, 9.554747, 2.488980, 113.576139);
}
function uranus_position(d)
{
  return planet_position(d, 1990, 84.039492, 271.063148, 172.884833, 0.046321, 19.218140, 0.773059, 73.926961);
}
function neptune_position(d)
{
   return planet_position(d, 1990, 164.792460, 282.349556, 48.009758, 0.009003, 30.109570, 1.770646, 131.670599);
}
function moon_position(d)
{
   // Lunar position model ELP2000-82 of Chapront.
   // The result values are for the equinox of date and have been adjusted
   // for light-time.
   // This is the simplified version of Jean Meeus, _Astronomical Algorithms_,
   // second edition, 1998, Willmann-Bell, Inc.
   //
   // [Meeus-1998: table 47.A]
   //    D, M, M1, F, l, r
   const _tblLR = new Array(
    new Array(0,  0,  1,  0, 6288774, -20905355),
    new Array(2,  0, -1,  0, 1274027,  -3699111),
    new Array(2,  0,  0,  0,  658314,  -2955968),
    new Array(0,  0,  2,  0,  213618,   -569925),
    new Array(0,  1,  0,  0, -185116,     48888),
    new Array(0,  0,  0,  2, -114332,     -3149),
    new Array(2,  0, -2,  0,   58793,    246158),
    new Array(2, -1, -1,  0,   57066,   -152138),
    new Array(2,  0,  1,  0,   53322,   -170733),
    new Array(2, -1,  0,  0,   45758,   -204586),
    new Array(0,  1, -1,  0,  -40923,   -129620),
    new Array(1,  0,  0,  0,  -34720,    108743),
    new Array(0,  1,  1,  0,  -30383,    104755),
    new Array(2,  0,  0, -2,   15327,     10321),
    new Array(0,  0,  1,  2,  -12528,         0),
    new Array(0,  0,  1, -2,   10980,     79661),
    new Array(4,  0, -1,  0,   10675,    -34782),
    new Array(0,  0,  3,  0,   10034,    -23210),
    new Array(4,  0, -2,  0,    8548,    -21636),
    new Array(2,  1, -1,  0,   -7888,     24208),
    new Array(2,  1,  0,  0,   -6766,     30824),
    new Array(1,  0, -1,  0,   -5163,     -8379),
    new Array(1,  1,  0,  0,    4987,    -16675),
    new Array(2, -1,  1,  0,    4036,    -12831),
    new Array(2,  0,  2,  0,    3994,    -10445),
    new Array(4,  0,  0,  0,    3861,    -11650),
    new Array(2,  0, -3,  0,    3665,     14403),
    new Array(0,  1, -2,  0,   -2689,     -7003),
    new Array(2,  0, -1,  2,   -2602,         0),
    new Array(2, -1, -2,  0,    2390,     10056),
    new Array(1,  0,  1,  0,   -2348,      6322),
    new Array(2, -2,  0,  0,    2236,     -9884),
    new Array(0,  1,  2,  0,   -2120,      5751),
    new Array(0,  2,  0,  0,   -2069,         0),
    new Array(2, -2, -1,  0,    2048,     -4950),
    new Array(2,  0,  1, -2,   -1773,      4130),
    new Array(2,  0,  0,  2,   -1595,         0),
    new Array(4, -1, -1,  0,    1215,     -3958),
    new Array(0,  0,  2,  2,   -1110,         0),
    new Array(3,  0, -1,  0,    -892,      3258),
    new Array(2,  1,  1,  0,    -810,      2616),
    new Array(4, -1, -2,  0,     759,     -1897),
    new Array(0,  2, -1,  0,    -713,     -2117),
    new Array(2,  2, -1,  0,    -700,      2354),
    new Array(2,  1, -2,  0,     691,         0),
    new Array(2, -1,  0, -2,     596,         0),
    new Array(4,  0,  1,  0,     549,     -1423),
    new Array(0,  0,  4,  0,     537,     -1117),
    new Array(4, -1,  0,  0,     520,     -1571),
    new Array(1,  0, -2,  0,    -487,     -1739),
    new Array(2,  1,  0, -2,    -399,         0),
    new Array(0,  0,  2, -2,    -381,     -4421),
    new Array(1,  1,  1,  0,     351,         0),
    new Array(3,  0, -2,  0,    -340,         0),
    new Array(4,  0, -3,  0,     330,         0),
    new Array(2, -1,  2,  0,     327,         0),
    new Array(0,  2,  1,  0,    -323,      1165),
    new Array(1,  1, -1,  0,     299,         0),
    new Array(2,  0,  3,  0,     294,         0),
    new Array(2,  0, -1, -2,       0,      8752));

   // [Meeus-1998: table 47.B]
   //    D, M, M1, F, b
   const _tblB = new Array(
    new Array(0,  0,  0,  1, 5128122),
    new Array(0,  0,  1,  1,  280602),
    new Array(0,  0,  1, -1,  277693),
    new Array(2,  0,  0, -1,  173237),
    new Array(2,  0, -1,  1,   55413),
    new Array(2,  0, -1, -1,   46271),
    new Array(2,  0,  0,  1,   32573),
    new Array(0,  0,  2,  1,   17198),
    new Array(2,  0,  1, -1,    9266),
    new Array(0,  0,  2, -1,    8822),
    new Array(2, -1,  0, -1,    8216),
    new Array(2,  0, -2, -1,    4324),
    new Array(2,  0,  1,  1,    4200),
    new Array(2,  1,  0, -1,   -3359),
    new Array(2, -1, -1,  1,    2463),
    new Array(2, -1,  0,  1,    2211),
    new Array(2, -1, -1, -1,    2065),
    new Array(0,  1, -1, -1,   -1870),
    new Array(4,  0, -1, -1,    1828),
    new Array(0,  1,  0,  1,   -1794),
    new Array(0,  0,  0,  3,   -1749),
    new Array(0,  1, -1,  1,   -1565),
    new Array(1,  0,  0,  1,   -1491),
    new Array(0,  1,  1,  1,   -1475),
    new Array(0,  1,  1, -1,   -1410),
    new Array(0,  1,  0, -1,   -1344),
    new Array(1,  0,  0, -1,   -1335),
    new Array(0,  0,  3,  1,    1107),
    new Array(4,  0,  0, -1,    1021),
    new Array(4,  0, -1,  1,     833),
    new Array(0,  0,  1, -3,     777),
    new Array(4,  0, -2,  1,     671),
    new Array(2,  0,  0, -3,     607),
    new Array(2,  0,  2, -1,     596),
    new Array(2, -1,  1, -1,     491),
    new Array(2,  0, -2,  1,    -451),
    new Array(0,  0,  3, -1,     439),
    new Array(2,  0,  2,  1,     422),
    new Array(2,  0, -3, -1,     421),
    new Array(2,  1, -1,  1,    -366),
    new Array(2,  1,  0,  1,    -351),
    new Array(4,  0,  0,  1,     331),
    new Array(2, -1,  1,  1,     315),
    new Array(2, -2,  0, -1,     302),
    new Array(0,  0,  1,  3,    -283),
    new Array(2,  1,  1, -1,    -229),
    new Array(1,  1,  0, -1,     223),
    new Array(1,  1,  0,  1,     223),
    new Array(0,  1, -2, -1,    -220),
    new Array(2,  1, -1, -1,    -220),
    new Array(1,  0,  1,  1,    -185),
    new Array(2, -1, -2, -1,     181),
    new Array(0,  1,  2,  1,    -177),
    new Array(4,  0, -2, -1,     176),
    new Array(4, -1, -1, -1,     166),
    new Array(1,  0,  1, -1,    -164),
    new Array(4,  0,  1, -1,     132),
    new Array(1,  0, -1, -1,    -119),
    new Array(4, -1,  0, -1,     115),
    new Array(2, -2,  0,  1,     107));

   const _kA1 = new Array(119.75, 131.849);
   const _kA2 = new Array(53.09, 479264.290);
   const _kA3 = new Array(313.45, 481266.484);

   const _kL1 = new Array(218.3164477, 481267.88123421, -0.0015786, 1.0/538841, -1.0/65194000);
   const _kD = new Array(297.8501921, 445267.1114034, -0.0018819, 1.0/545868, -1.0/113065000);
   const _kM1 = new Array(134.9633964, 477198.8675055, 0.0087414, 1.0/69699, -1.0/14712000);
   const _kF = new Array(93.2720950, 483202.0175233, -0.0036539, -1.0/3526000, 1.0/863310000);
   const _ko = new Array(125.0445479, -1934.1362891, 0.0020754, 1.0/467441.0, 1.0/60616000.0);
   const _kM = new Array(357.5291092, 35999.0502909, -0.0001536, 1.0/24490000);

   // get Julian day number and Julian century number since J2000.0
   var jd = date2jd(d);
   var T = jd2jcent(jd);

   // common terms
   var L1 = deg2rad(mod360(polynomial(_kL1, T)));
   var D = deg2rad(mod360(polynomial(_kD, T)));
   var M = deg2rad(mod360(polynomial(_kM, T)));
   var M1 = deg2rad(mod360(polynomial(_kM1, T)));
   var F = deg2rad(mod360(polynomial(_kF, T)));
   var A1 = deg2rad(mod360(polynomial(_kA1, T)));
   var A2 = deg2rad(mod360(polynomial(_kA2, T)));
   var A3 = deg2rad(mod360(polynomial(_kA3, T)));
   var E = polynomial(new Array(1.0, -0.002516, -0.0000074), T)
   var E2 = E*E

   // geocentric ecliptic longitude
   var lsum = 0;
   for (var [tD, tM, tM1, tF, tl, tr] of _tblLR)
   {
      var arg = tD * D + tM * M + tM1 * M1 + tF * F;
      if (Math.abs(tM) == 1)
      {
         tl *= E;
      }
      else if (Math.abs(tM) == 2)
      {
         tl *= E2;
      }
      lsum += tl * Math.sin(arg);
   }
   lsum +=  3958*Math.sin(A1)
           +1962*Math.sin(L1 - F)
           +318*Math.sin(A2);
   var ecl_lon = rad2deg(L1) + lsum / 1000000;

   // geocentric ecliptic latitude
   var bsum = 0.0;
   for (var [tD, tM, tM1, tF, tb] of _tblB)
   {
      var arg = tD * D + tM * M + tM1 * M1 + tF * F;
      if (Math.abs(tM) == 1)
      {
         tb *= E;
      }
      else if (Math.abs(tM) == 2)
      {
         tb *= E2;
      }
      bsum += tb * Math.sin(arg);
   }
   bsum += -2235 * Math.sin(L1)
           +382 * Math.sin(A3)
           +175 * Math.sin(A1 - F)
           +175 * Math.sin(A1 + F)
           +127 * Math.sin(L1 - M1)
           -115 * Math.sin(L1 + M1);
   var ecl_lat = bsum / 1000000;

   return ecliptic2equatorial(jd, ecl_lat, ecl_lon);
}

// -------------------------------------
//
// Celestial object management
//
// -------------------------------------

// Manage a list of pre-defined celestial objects
function CelestialObject(name, ra_h, ra_min, ra_sec, dec_deg, dec_min, dec_sec)
{
   this.name = name;
   this.ra = hms2hr(ra_h, ra_min, ra_sec);
   this.dec = dms2deg(dec_deg, dec_min, dec_sec);
}
CelestialObject.prototype.position = function(d)
{
   return correct_precession(d, this.ra, this.dec);
}

const CELESTIAL_OBJECTS = new Array(
   new CelestialObject("<Custom coordinates>", 0, 0, 0, 0, 0, 0),
   new Planet("Sun", "Soleil", sun_position),
   new Planet("Mercury", "Mercure", mercury_position),
   new Planet("Venus", "Venus", venus_position),
   new Planet("Moon", "Lune", moon_position),
   new Planet("Mars", "Mars", mars_position),
   new Planet("Jupiter", "Jupiter", jupiter_position),
   new Planet("Saturn", "Saturne", saturn_position),
   new Planet("Uranus", "Uranus", uranus_position),
   new Planet("Neptune", "Neptune", neptune_position),
   new CelestialObject("Cassiopeia A", 23, 23, 24, 58, 47, 54),
   new CelestialObject("S7", 2, 6, 12, 60, 32, 54),
   new CelestialObject("S8", 5, 47, 21, -1, 40, 18),
   new CelestialObject("S9", 17, 52, 5, -34, 25, 15),
   new CelestialObject("M1, NGC 1952, LBN 833, Sh2-244, CED 53, Taurus A", 5, 34, 31, 22, 0, 52),
   new CelestialObject("M2, NGC 7089, GCL 121", 21, 33, 27, 0, 49, 22),
   new CelestialObject("M3, NGC 5272, GCL 25", 13, 42, 11, 28, 22, 34),
   new CelestialObject("M4, NGC 6121, GCL 41, ESO 517-SC1", 16, 23, 35, -26, 31, 29),
   new CelestialObject("M5, NGC 5904, GCL 34", 15, 18, 33, 2, 5, 0),
   new CelestialObject("M6, NGC 6405, OCL 1030, ESO 455-SC30, Butterfly cluster", 17, 40, 20, -32, 15, 0),
   new CelestialObject("M7, NGC 6475, OCL 1028, ESO 394-SC9, Ptolemy's cluster", 17, 53, 50, -34, 47, 36),
   new CelestialObject("M8, NGC 6523, LBN 25, Lagoon nebula", 18, 3, 42, -24, 22, 48),
   new CelestialObject("M9, NGC 6333, GCL 60, ESO 587-SC5", 17, 19, 11, -18, 30, 57),
   new CelestialObject("M10, NGC 6254, GCL 49", 16, 57, 8, -4, 5, 56),
   new CelestialObject("M11, NGC 6705, OCL 76, Wild Duck cluster", 18, 51, 5, -6, 16, 12),
   new CelestialObject("M12, NGC 6218, GCL 46", 16, 47, 14, -1, 56, 50),
   new CelestialObject("M13, NGC 6205, GCL 45, Hercules cluster", 16, 41, 41, 36, 27, 39),
   new CelestialObject("M14, NGC 6402, GCL 72", 17, 37, 36, -3, 14, 43),
   new CelestialObject("M15, NGC 7078, GCL 120", 21, 29, 58, 12, 10, 3),
   new CelestialObject("M16, NGC 6611, OCL 54, LBN 67, with Eagle nebula", 18, 18, 48, -13, 48, 24),
   new CelestialObject("M17, NGC 6618, OCL 44, LBN 60, Sh2-45", 18, 20, 47, -16, 10, 18),
   new CelestialObject("M18, NGC 6613, OCL 40", 18, 19, 58, -17, 6, 6),
   new CelestialObject("M19, NGC 6273, GCL 52, ESO 518-SC7", 17, 2, 37, -26, 16, 3),
   new CelestialObject("M20, NGC 6514, OCL 23, ESO 521-N*13, LBN 27", 18, 2, 42, -22, 58, 18),
   new CelestialObject("M21, NGC 6531, OCL 26, ESO 521-SC19", 18, 4, 13, -22, 29, 24),
   new CelestialObject("M22, NGC 6656, GCL 99, ESO 523-SC4", 18, 36, 24, -23, 54, 10),
   new CelestialObject("M23, NGC 6494, OCL 30, ESO 589-SC22", 17, 57, 4, -18, 59, 0),
   new CelestialObject("M24, IC 4715, ESO 591-**1", 18, 18, 48, -18, 33, 0),
   new CelestialObject("M25, IC 4725, OCL 38, ESO 591-SC6", 18, 31, 48, -19, 6, 48),
   new CelestialObject("M26, NGC 6694, OCL 67", 18, 45, 18, -9, 22, 50),
   new CelestialObject("M27, NGC 6853, PK 60+3.1", 19, 59, 36, 22, 43, 18),
   new CelestialObject("M28, NGC 6626, GCL 94, ESO 522-SC23", 18, 24, 32, -24, 52, 10),
   new CelestialObject("M29, NGC 6913, OCL 168", 20, 23, 58, 38, 30, 30),
   new CelestialObject("M30, NGC 7099, GCL 122, ESO 531-SC21", 21, 40, 22, -23, 10, 43),
   new CelestialObject("M31, NGC 224, UGC 454, MCG 7-2-16, ZWG 535.17, PGC2557 N", 0, 42, 44, 41, 16, 8),
   new CelestialObject("M32, NGC 221, UGC 452, MCG 7-2-15, ZWG 535.16, PGC2555 N", 0, 42, 41, 40, 51, 57),
   new CelestialObject("M33, NGC 598, UGC 1117, MCG 5-4-69, ZWG 502.110, PGC5818 N", 1, 33, 51, 30, 39, 29),
   new CelestialObject("M34, NGC 1039, OCL 382", 2, 42, 5, 42, 45, 42),
   new CelestialObject("M35, NGC 2168, OCL 466", 6, 9, 0, 24, 21, 0),
   new CelestialObject("M36, NGC 1960, OCL 445", 5, 36, 17, 34, 8, 27),
   new CelestialObject("M37, NGC 2099, OCL 451", 5, 52, 18, 32, 33, 11),
   new CelestialObject("M38, NGC 1912, OCL 433", 5, 28, 40, 35, 50, 54),
   new CelestialObject("M39, NGC 7092, OCL 211", 21, 31, 42, 48, 25, 0),
   new CelestialObject("M40", 12, 22, 15, 58, 5, 4),
   new CelestialObject("M41, NGC 2287, OCL 597, ESO 557-SC14", 6, 46, 0, -20, 45, 24),
   new CelestialObject("M42, NGC 1976, LBN 974, Orion nebula", 5, 35, 17, -5, 23, 25),
   new CelestialObject("M43, NGC 1982, CED 55G", 5, 35, 31, -5, 16, 3),
   new CelestialObject("M44, NGC 2632, OCL 507, Praesepe", 8, 39, 57, 19, 40, 21),
   new CelestialObject("M45", 3, 47, 0, 24, 7, 0),
   new CelestialObject("M46, NGC 2437, OCL 601", 7, 41, 46, -14, 48, 36),
   new CelestialObject("M47, NGC 2422, NGC 2478, OCL 596", 7, 36, 35, -14, 28, 47),
   new CelestialObject("M48, NGC 2548, OCL 584", 8, 13, 43, -5, 45, 2),
   new CelestialObject("M49, NGC 4472, UGC 7629, MCG 1-32-83, ZWG 42.134, PGC41220 N", 12, 29, 46, 8, 0, 0),
   new CelestialObject("M50, NGC 2323, OCL 559", 7, 2, 42, -8, 23, 0),
   new CelestialObject("M51, NGC 5194, UGC 8493, MCG 8-25-12, Whirlpool galaxy, PGC47404 N", 13, 29, 52, 47, 11, 44),
   new CelestialObject("M52, NGC 7654, OCL 260", 23, 24, 48, 61, 35, 36),
   new CelestialObject("M53, NGC 5024, GCL 22", 13, 12, 55, 18, 10, 11),
   new CelestialObject("M54, NGC 6715, GCL 104, ESO 458-SC8", 18, 55, 3, -30, 28, 40),
   new CelestialObject("M55, NGC 6809, GCL 113, ESO 460-SC21", 19, 39, 59, -30, 57, 42),
   new CelestialObject("M56, NGC 6779, GCL 110", 19, 16, 35, 30, 11, 7),
   new CelestialObject("M57, NGC 6720, PK 63+13.1", 18, 53, 35, 33, 1, 47),
   new CelestialObject("M58, NGC 4579, UGC 7796, MCG 2-32-160, VCC 1727, PGC42168 N", 12, 37, 43, 11, 49, 6),
   new CelestialObject("M59, NGC 4621, UGC 7858, MCG 2-32-183, ZWG 70.223, PGC42628 N", 12, 42, 2, 11, 38, 50),
   new CelestialObject("M60, NGC 4649, UGC 7898, MCG 2-33-2, KCPG 353B, PGC42831 N", 12, 43, 39, 11, 33, 11),
   new CelestialObject("M61, NGC 4303, UGC 7420, MCG 1-32-22, VCC 508, PGC40001 N", 12, 21, 54, 4, 28, 22),
   new CelestialObject("M62, NGC 6266, GCL 51, ESO 453-SC14", 17, 1, 12, -30, 6, 42),
   new CelestialObject("M63, NGC 5055, UGC 8334, MCG 7-27-54, Sunflower galaxy, PGC46153 N", 13, 15, 49, 42, 1, 59),
   new CelestialObject("M64, NGC 4826, UGC 8062, MCG 4-31-1, Black Eye galaxy, PGC44182 N", 12, 56, 43, 21, 40, 59),
   new CelestialObject("M65, NGC 3623, UGC 6328, MCG 2-29-18, ARP 317, PGC34612 N", 11, 18, 55, 13, 5, 27),
   new CelestialObject("M66, NGC 3627, UGC 6346, MCG 2-29-19, ARP 317, PGC34695 N", 11, 20, 15, 12, 59, 24),
   new CelestialObject("M67, NGC 2682, OCL 549", 8, 51, 0, 11, 48, 0),
   new CelestialObject("M68, NGC 4590, GCL 20, ESO 506-SC30", 12, 39, 28, -26, 44, 32),
   new CelestialObject("M69, NGC 6634, NGC 6637, GCL 96, ESO 457-SC14", 18, 31, 23, -32, 20, 51),
   new CelestialObject("M70, NGC 6681, GCL 101, ESO 458-SC3", 18, 43, 12, -32, 17, 29),
   new CelestialObject("M71, NGC 6838, GCL 115", 19, 53, 46, 18, 46, 44),
   new CelestialObject("M72, NGC 6981, GCL 118", 20, 53, 27, -12, 32, 11),
   new CelestialObject("M73, NGC 6994, OCL 89", 20, 58, 56, -12, 38, 7),
   new CelestialObject("M74, NGC 628, UGC 1149, MCG 3-5-11, ZWG 460.14, PGC5974 N", 1, 36, 41, 15, 47, 0),
   new CelestialObject("M75, NGC 6864, GCL 116, ESO 595-SC13", 20, 6, 4, -21, 55, 15),
   new CelestialObject("M76, NGC 650, NGC 651, PK 130-10.1", 1, 42, 18, 51, 34, 17),
   new CelestialObject("M77, NGC 1068, UGC 2188, MCG 0-7-83, ZWG 388.98, PGC10266 N", 2, 42, 40, 0, 0, 46),
   new CelestialObject("M78, NGC 2068, DG 80", 5, 46, 45, 0, 4, 48),
   new CelestialObject("M79, NGC 1904, ESO 487-SC7, GCL 10", 5, 24, 10, -24, 31, 25),
   new CelestialObject("M80, NGC 6093, GCL 39, ESO 516-SC11", 16, 17, 2, -22, 58, 28),
   new CelestialObject("M81, NGC 3031, UGC 5318, MCG 12-10-10, Bode's nebulae, PGC28630 N", 9, 55, 33, 69, 4, 2),
   new CelestialObject("M82, NGC 3034, UGC 5322, MCG 12-10-11, ARP 337, Ursa Major A, PGC28655 N", 9, 55, 54, 69, 40, 59),
   new CelestialObject("M83, NGC 5236, ESO 444-81, MCG -5-32-50, UGCA 366, PGC48082 N", 13, 37, 0, -29, 52, 2),
   new CelestialObject("M84, NGC 4374, UGC 7494, MCG 2-32-34, PGC40455 N", 12, 25, 3, 12, 53, 13),
   new CelestialObject("M85, NGC 4382, UGC 7508, MCG 3-32-29, KCPG 334A, VCC 798, PGC40515 N", 12, 25, 23, 18, 11, 27),
   new CelestialObject("M86, NGC 4406, UGC 7532, MCG 2-32-46, ZWG 70.72, PGC40653 N", 12, 26, 11, 12, 56, 47),
   new CelestialObject("M87, NGC 4486, UGC 7654, MCG 2-32-105, 3C 274, PGC41361 N", 12, 30, 49, 12, 23, 26),
   new CelestialObject("M88, NGC 4501, UGC 7675, MCG 3-32-59, ZWG 99.76, PGC41517 N", 12, 31, 59, 14, 25, 11),
   new CelestialObject("M89, NGC 4552, UGC 7760, MCG 2-32-149, ZWG 70.184, PGC41968 N", 12, 35, 39, 12, 33, 22),
   new CelestialObject("M90, NGC 4569, UGC 7786, MCG 2-32-155, IRAS12343+1326, PGC42089 N", 12, 36, 50, 13, 9, 50),
   new CelestialObject("M91, NGC 4548, UGC 7753, MCG 3-32-75, VCC 1615, PGC41934 N", 12, 35, 26, 14, 29, 47),
   new CelestialObject("M92, NGC 6341, GCL 59", 17, 17, 7, 43, 8, 13),
   new CelestialObject("M93, NGC 2447, OCL 649, ESO 493-SC7", 7, 44, 30, -23, 51, 24),
   new CelestialObject("M94, NGC 4736, UGC 7996, MCG 7-26-58, ZWG 217.1, PGC43495 N", 12, 50, 53, 41, 7, 17),
   new CelestialObject("M95, NGC 3351, UGC 5850, MCG 2-28-1, ZWG 66.4, PGC32007 N", 10, 43, 57, 11, 42, 12),
   new CelestialObject("M96, NGC 3368, UGC 5882, MCG 2-28-6, ZWG 66.13, PGC32192 N", 10, 46, 45, 11, 49, 12),
   new CelestialObject("M97, NGC 3587, PK 148+57.1", 11, 14, 47, 55, 1, 10),
   new CelestialObject("M98, NGC 4192, UGC 7231, MCG 3-31-79, VCC 92, PGC39028 N", 12, 13, 47, 14, 53, 58),
   new CelestialObject("M99, NGC 4254, UGC 7345, MCG 3-31-99, Pinwheel galaxy, PGC39578 N", 12, 18, 49, 14, 25, 3),
   new CelestialObject("M100, NGC 4321, UGC 7450, MCG 3-32-15, KUG 1220+160, PGC40153 N", 12, 22, 54, 15, 49, 22),
   new CelestialObject("M101, NGC 5457, UGC 8981, MCG 9-23-28, VV 344, PGC50063 N", 14, 3, 12, 54, 20, 58),
   new CelestialObject("M102, NGC 5866, UGC 9723, MCG 9-25-17, ZWG 274.16, PGC53933 N", 15, 6, 29, 55, 45, 49),
   new CelestialObject("M103, NGC 581, OCL 326", 1, 33, 23, 60, 39, 0),
   new CelestialObject("M104, NGC 4594, MCG -2-32-20, UGCA 293, Sombrero galaxy, PGC42407 N", 12, 39, 59, -11, 37, 21),
   new CelestialObject("M105, NGC 3379, UGC 5902, MCG 2-28-11, ZWG 66.18, PGC32256 N", 10, 47, 49, 12, 34, 52),
   new CelestialObject("M106, NGC 4258, UGC 7353, MCG 8-22-104, ZWG 244.3, PGC39600 N", 12, 18, 57, 47, 18, 25),
   new CelestialObject("M107, NGC 6171, GCL 44", 16, 32, 31, -13, 3, 11),
   new CelestialObject("M108, NGC 3556, UGC 6225, MCG 9-18-98, KARA 469, PGC34030 N", 11, 11, 29, 55, 40, 22),
   new CelestialObject("M109, NGC 3992, UGC 6937, MCG 9-20-44, ZWG 269.23, PGC37617 N", 11, 57, 35, 53, 22, 25),
   new CelestialObject("M110, NGC 205, UGC 426, MCG 7-2-14, ZWG 535.14, PGC2429 N", 0, 40, 22, 41, 41, 7)
);

function populate_object_select(object_select)
{
   object_select.options.length = 0;
   for(var i = 0; i < CELESTIAL_OBJECTS.length; i++)
   {
      object_select.options[i] = new Option(CELESTIAL_OBJECTS[i].name, i, false, false);
   }
}


